package com.livepeak.android.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.KeyEvent;

import com.livepeak.mobile.R;

/**
 * Created by guillaume on 08/08/15.
 */
public class LocationProviderDelegate {

    private final Activity mActivity;

    private LocationManager mLocationManager;

    public LocationProviderDelegate(Activity activity) {
        mActivity = activity;
    }

    public void onCreate() {
        mLocationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
    }

    public void onResume() {
        boolean providerEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(!providerEnabled){
            DialogFragment dialogFragment = AlertDialogFragment.newInstance();
            dialogFragment.show(mActivity.getFragmentManager(), "dialog");
        }
    }

    public static class AlertDialogFragment extends DialogFragment {

        public static AlertDialogFragment newInstance() {
            AlertDialogFragment fragment = new AlertDialogFragment();
            return fragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            OnClickListener listener = new OnClickListener();

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.app_location_provider_dialog_title);
            builder.setMessage(R.string.app_location_provider_dialog_message);
            builder.setPositiveButton(R.string.app_location_provider_dialog_ok_button, listener);
            builder.setOnKeyListener(new OnKeyListener());
            return builder.create();
        }

        private class OnClickListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                switch (whichButton) {
                    case DialogInterface.BUTTON_POSITIVE:
                        getActivity().startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        break;
                }
            }
        }

        private class OnKeyListener implements DialogInterface.OnKeyListener {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        ((Listener) getActivity()).onLocationProviderDisabled();
                        return true;
                    default:
                        return false;
                }
            }
        }
    }

    public interface Listener {
        void onLocationProviderDisabled();
    }
}
