package com.livepeak.android.database;

import android.database.Cursor;

/**
 * Created by guillaume on 02/05/15.
 */
public class CursorHolder {

    private Cursor mCursor;

    public final int idIndex;

    public CursorHolder(Cursor cursor) {
        mCursor = cursor;
        this.idIndex = cursor.getColumnIndex("_id");
    }

    public Cursor getCursor() {
        return mCursor;
    }
}
