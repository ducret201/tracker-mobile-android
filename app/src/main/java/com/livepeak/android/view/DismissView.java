package com.livepeak.android.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by guillaume on 16/01/15.
 */
public class DismissView extends FrameLayout {

    private Gesture mGesture;

    protected OnDismissListener mlistener;

    public DismissView(Context context) {
        super(context);
        initDismissView();
    }

    public DismissView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initDismissView();
    }

    public DismissView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initDismissView();
    }

    private void initDismissView() {
        mGesture = new Gesture();
        setOnTouchListener(mGesture);
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        getDismissView().setOnClickListener(listener);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return mGesture.onInterceptTouchEvent(event);
    }

    public void setOnDismissListener(OnDismissListener listener) {
        mlistener = listener;
    }

    public void allowUndo() {
        if (getUndoView() != null) {
            getBackgroundView().setVisibility(INVISIBLE);
        }
    }

    public void reset() {
        reset(true);
    }

    public void reset(boolean animate) {
        if (animate) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(getDismissView(), "translationX", 0f);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mGesture.enable();

                    View view = getBackgroundView();
                    if (view != null) {
                        view.setVisibility(VISIBLE);
                    }
                }
            });
            animator.setDuration(200).start();
        } else {
            mGesture.enable();
            getDismissView().setTranslationX(0f);
            View view = getBackgroundView();
            if (view != null) {
                view.setVisibility(VISIBLE);
            }
        }
    }

    private View getDismissView() {
        return getChildAt(getChildCount() - 1);
    }

    private View getBackgroundView() {
        if (getChildCount() > 1) return getChildAt(getChildCount() - 2);
        return null;
    }

    private View getUndoView() {
        if (getChildCount() > 2) return getChildAt(getChildCount() - 3);
        return null;
    }

    public interface OnDismissListener {

        void onDismiss(DismissView view);

        void onUndo(DismissView view);
    }

    private class Gesture extends GestureDetector.SimpleOnGestureListener implements OnTouchListener {

        private GestureDetector mGestureDetector = new GestureDetector(getContext(), this);

        private boolean mEnabled = true;

        private boolean mScrolling = false;

        private float mScrollingDelta;

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (!mEnabled) return false;

            switch(event.getAction()) {
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    if (mScrolling) {
                        onScrollEnd();
                        mScrolling = false;
                        mScrollingDelta = 0f;
                    } else {
                        performClick();
                    }
                    break;
            }

            return mGestureDetector.onTouchEvent(event);
        }

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        public boolean onInterceptTouchEvent(MotionEvent event) {
            if (!mEnabled) return false;

            mGestureDetector.onTouchEvent(event);
            return mScrolling;
        }

        public void enable() {
            mEnabled = true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            getDismissView().setTranslationX(mScrollingDelta = (e2.getX() - e1.getX()));
            return mScrolling = true;
        }

        private void onScrollEnd() {
            if (Math.abs(mScrollingDelta) > getWidth() / 3) {
                mEnabled = false;

                // dismiss
                float endValue = getWidth();
                if (mScrollingDelta < 0) {
                    endValue *= -1;
                }

                Animator translateAnimator = ObjectAnimator.ofFloat(getDismissView(), "translationX", endValue);

                //ObjectAnimator anim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
                //anim.setDuration(1000);
                //anim.start();

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(translateAnimator);
                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (mlistener != null) {
                            mlistener.onDismiss(DismissView.this);
                        }
                    }
                });
                animatorSet.setDuration(200).start();
            } else {
                // reset
                ObjectAnimator.ofFloat(getDismissView(), "translationX", 0f).setDuration(200).start();
            }
        }
    }
}
