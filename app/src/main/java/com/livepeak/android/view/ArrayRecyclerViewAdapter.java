package com.livepeak.android.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

/**
 * Created by guillaume on 03/09/15.
 */
public abstract class ArrayRecyclerViewAdapter<VH extends RecyclerView.ViewHolder, T> extends RecyclerView.Adapter<VH> {

    private List<T> mObjects;

    public ArrayRecyclerViewAdapter(Context context, T[] array) {
        mObjects = Arrays.asList(array);
    }
    
    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    public T getItem(int position) {
        return mObjects.get(position);
    }

}
