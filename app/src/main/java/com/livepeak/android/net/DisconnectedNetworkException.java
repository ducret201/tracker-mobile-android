package com.livepeak.android.net;

import java.io.IOException;

/**
 * Created by guillaume on 18/05/15.
 */
public class DisconnectedNetworkException extends IOException {

    public DisconnectedNetworkException() {
    }

    public DisconnectedNetworkException(Throwable cause) {
        super(cause);
    }

    public DisconnectedNetworkException(String detailMessage) {
        super(detailMessage);
    }

    public DisconnectedNetworkException(String message, Throwable cause) {
        super(message, cause);
    }
}
