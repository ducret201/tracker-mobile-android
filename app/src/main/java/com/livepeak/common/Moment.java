package com.livepeak.common;

import java.util.Date;

/**
 * Created by guillaume on 22/07/15.
 */
public class Moment {
    private long mValue;

    public Moment(long value) {
        mValue = value;
    }

    public String fromNow() {
        long timestamp = (new Date().getTime() - mValue) / 1000; // seconds
        boolean past = timestamp >= 0;
        timestamp = Math.abs(timestamp);
        long hours = timestamp / 3600;
        timestamp = timestamp % 3600;
        long minutes = timestamp / 60;
        timestamp = timestamp % 60;
        long seconds = timestamp;

        String prefix = past ? "" : "in ";
        String suffix = past ? " ago" : "";
        String formattedValue;
        String formattedUnit;

        if (hours > 0) {
            formattedValue = Long.toString(hours);
            formattedUnit = "hours";
        } else if (minutes > 0) {
            formattedValue = Long.toString(minutes);
            formattedUnit = "minutes";
        } else {
            formattedValue = "a few";
            formattedUnit = "seconds";
        }

        return prefix + formattedValue + " " + formattedUnit + suffix;
    }

}
