package com.livepeak.common;

import android.util.Base64;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Created by guillaume on 18/08/15.
 */
public final class ShortId {

    public static String generate() {
        UUID uuid = UUID.randomUUID();
        ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
        buffer.putLong(uuid.getMostSignificantBits());
        buffer.putLong(uuid.getLeastSignificantBits());
        String base64Uuid = Base64.encodeToString(buffer.array(), Base64.URL_SAFE | Base64.NO_WRAP);
        return base64Uuid.replaceAll("=+$", "");
    }
}
