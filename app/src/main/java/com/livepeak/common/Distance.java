package com.livepeak.common;

/**
 * Created by guillaume on 07/07/15.
 */
public class Distance {

    public static final String UNIT_M = "m";

    public static final String UNIT_KM = "km";

    private long mValue;

    public Distance(long value) {
        mValue = value;
    }

    public String format(String[] result) {
        if (mValue / 1000 >= 1) return format(UNIT_KM, result);
        return format(UNIT_M, result);
    }

    public String format(String unit, String[] result) {
        String formattedValue = null;

        if (UNIT_KM.equals(unit)) {
            formattedValue = Float.toString(Math.round(mValue / 100) / 10f);
        } else if (UNIT_M.equals(unit)) {
            formattedValue = Long.toString(Math.round(mValue));
        }

        if (result != null && result.length > 0) {
            result[0] = formattedValue;
        }
        if (result != null && result.length > 1) {
            result[1] = unit;
        }
        return formattedValue + " " + unit;
    }

}
