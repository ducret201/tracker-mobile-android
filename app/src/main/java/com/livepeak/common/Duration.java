package com.livepeak.common;

/**
 * Created by guillaume on 07/07/15.
 */
public class Duration {

    private long mValue;

    public Duration(long value) {
        mValue = value;
    }

    public String format(String[] result) {
        long timestamp = mValue / 1000; // seconds
        long hours = timestamp / 3600;
        timestamp = timestamp % 3600;
        long minutes = timestamp / 60;
        timestamp = timestamp % 60;
        long seconds = timestamp;

        String formattedValue = String.format("%02d:%02d", hours, minutes);
        String formattedUnit = "h:m";

        if (result != null && result.length > 0) {
            result[0] = formattedValue;
        }
        if (result != null && result.length > 1) {
            result[1] = formattedUnit;
        }
        return formattedValue + " " + formattedUnit;
    }
}
