package com.livepeak.mobile.app;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.livepeak.mobile.profile.ProfileDatasource;
import com.livepeak.mobile.track.AnalyseService;
import com.livepeak.mobile.track.GatewayService;
import com.livepeak.mobile.track.PositionService;
import com.livepeak.mobile.track.TrackDatasource;

/**
 * Created by guillaume on 20/01/15.
 */
public class TrackerApplication extends Application {

    public static final String APP_PREFS = "APP";

    public static final String PREFS_USERNAME = "USERNAME";

    public static final String PREFS_GPS_TIME_DRIFT = "GPS_TIME_DRIFT";

    private ProfileDatasource mProfileDatasource;

    private TrackDatasource mTrackDatasource;

    @Override
    public void onCreate() {
        super.onCreate();
        registerApplicationReceivers();
    }

    public synchronized ProfileDatasource getProfileDatasource() {
        if (mProfileDatasource == null) {
            mProfileDatasource = new ProfileDatasource(this);
        }
        return mProfileDatasource;
    }

    public synchronized TrackDatasource getTrackDatasource() {
        if (mTrackDatasource == null) {
            mTrackDatasource = new TrackDatasource(this);
        }
        return mTrackDatasource;
    }

    private void registerApplicationReceivers() {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        BroadcastReceiver gatewayServiceReceiver = new GatewayService.Receiver();
        BroadcastReceiver analyseMetricsServiceReceiver = new AnalyseService.Receiver(AnalyseService.ANALYSE_METRICS);
        BroadcastReceiver analyseStateServiceReceiver = new AnalyseService.Receiver(AnalyseService.ANALYSE_STATE);

        // register position received receivers
        IntentFilter filter = new IntentFilter(PositionService.BROADCAST_ACTION_RECEIVED);
        broadcastManager.registerReceiver(gatewayServiceReceiver, filter);
        broadcastManager.registerReceiver(analyseMetricsServiceReceiver, filter);

        // register gateway sent receivers
        filter = new IntentFilter();
        filter.addAction(GatewayService.BROADCAST_ACTION_TRACK_SENT);
        filter.addAction(GatewayService.BROADCAST_ACTION_POSITION_SENT);
        filter.addAction(GatewayService.BROADCAST_ACTION_FAILED);
        broadcastManager.registerReceiver(analyseStateServiceReceiver, filter);
    }
}
