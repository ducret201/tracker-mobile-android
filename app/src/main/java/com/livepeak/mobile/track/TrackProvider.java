package com.livepeak.mobile.track;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.livepeak.mobile.app.TrackerApplication;

public class TrackProvider extends ContentProvider {

    private static final int TRACK_TYPE = 10;

    private static final int TRACK_ITEM_TYPE = 20;

    private static final int POSITION_TYPE = 30;

    private static final int POSITION_ITEM_TYPE = 40;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private TrackDatasource mDatasource;

    static {
        sURIMatcher.addURI(TracksContract.AUTHORITY, TracksContract.BASE_PATH, TRACK_TYPE);
        sURIMatcher.addURI(TracksContract.AUTHORITY, TracksContract.BASE_PATH + "/#", TRACK_ITEM_TYPE);
        sURIMatcher.addURI(TracksContract.AUTHORITY, TracksContract.Position.BASE_PATH, POSITION_TYPE);
        sURIMatcher.addURI(TracksContract.AUTHORITY, TracksContract.Position.BASE_PATH + "/#", POSITION_ITEM_TYPE);
    }

    @Override
    public boolean onCreate() {
        mDatasource = ((TrackerApplication) getContext().getApplicationContext()).getTrackDatasource();
        return false;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String limit = uri.getQueryParameter("LIMIT");
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case TRACK_TYPE:
                queryBuilder.setTables(TrackDatasource.TABLE_TRACK);
                break;
            case TRACK_ITEM_TYPE:
                queryBuilder.setTables(TrackDatasource.TABLE_TRACK);
                queryBuilder.appendWhere(TrackDatasource.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case POSITION_TYPE:
                queryBuilder.setTables(TrackDatasource.TABLE_POSITION);
                break;
            case POSITION_ITEM_TYPE:
                queryBuilder.setTables(TrackDatasource.TABLE_POSITION);
                queryBuilder.appendWhere(TrackDatasource.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = mDatasource.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder, limit);

        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
