package com.livepeak.mobile.track;

import android.content.Context;
import android.util.AttributeSet;


/**
 * Created by guillaume on 22/07/15.
 */
public class DurationMetricView extends MetricView {

    private long mBeginTimestamp = -1;

    public DurationMetricView(Context context) {
        super(context);
        init();
    }

    public DurationMetricView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DurationMetricView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setType(TYPE_DURATION);
    }

    public void setBeginTimestamp(long timestamp) {
        mBeginTimestamp = timestamp;
        refresh();
    }

    public void refresh() {
        if (mBeginTimestamp >= 0) {
            setValue(System.currentTimeMillis() - mBeginTimestamp);
        }
    }
}
