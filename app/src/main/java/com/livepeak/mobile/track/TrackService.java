package com.livepeak.mobile.track;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.livepeak.common.ShortId;
import com.livepeak.mobile.R;
import com.livepeak.mobile.app.TrackerApplication;
import com.livepeak.mobile.profile.Profile;

/**
 * Created by guillaume on 11/01/15.
 */
public class TrackService extends IntentService {

    private static final String TAG = TrackService.class.getSimpleName();

    public final static String BROADCAST_ACTION_STARTED = TrackService.class.getPackage().getName() + ".intent.action.STARTED";

    public final static String BROADCAST_ACTION_STOPPED = TrackService.class.getPackage().getName() + ".intent.action.STOPPED";

    public final static String EXTRA_ACTION = "ACTION";

    public final static String EXTRA_PROFILE = "PROFILE";

    public final static String EXTRA_TRACK_ID = "TRACK_ID";

    public final static int ACTION_START_TRACKING = 10;

    public final static int ACTION_STOP_TRACKING = 20;

    public final static int MIN_DISTANCE = -1;

    private TrackDatasource mDatasource;

    private LocationManager mLocationManager;

    private NotificationManager mNotificationManager;

    public TrackService() {
        super(TrackService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mDatasource = ((TrackerApplication) getApplication()).getTrackDatasource();
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long trackId;

        switch (intent.getIntExtra(EXTRA_ACTION, ACTION_START_TRACKING)) {
            case ACTION_START_TRACKING:
                Profile profile = intent.getParcelableExtra(EXTRA_PROFILE);
                trackId = createTrack(profile);

                startTracking(trackId, profile);
                createNotification(trackId, profile);
                Log.d(TAG, "Tracking started for track {_id: " + trackId + "}");

                // broadcast
                Intent broadcastIntent = new Intent(BROADCAST_ACTION_STARTED);
                broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                break;
            case ACTION_STOP_TRACKING:
                trackId = intent.getLongExtra(EXTRA_TRACK_ID, -1);

                stopTracking(trackId);
                cancelNotification(trackId);
                Log.d(TAG, "Tracking stopped for track {_id: " + trackId + "}");

                // broadcast
                broadcastIntent = new Intent(BROADCAST_ACTION_STOPPED);
                broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                break;
        }
    }

    private void startTracking(long trackId, Profile profile) {
        Intent intent = new Intent(this, PositionService.class);
        intent.putExtra(PositionService.EXTRA_TRACK_ID, trackId);
        PendingIntent pendingIntent = PendingIntent.getService(this, (int) trackId, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, profile.getFrequency() * 1000, MIN_DISTANCE, pendingIntent);
    }

    private void stopTracking(long trackId) {
        // stop position updates
        Intent intent = new Intent(this, PositionService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, (int) trackId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        mLocationManager.removeUpdates(pendingIntent);

        // request last position
        intent = new Intent(this, PositionService.class);
        intent.putExtra(PositionService.EXTRA_TRACK_ID, trackId);
        intent.putExtra(PositionService.EXTRA_FLAG, TracksContract.Position.FLAG_STOP);
        pendingIntent = PendingIntent.getService(this, (int) trackId, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, pendingIntent);
    }

    private long createTrack(Profile profile) {
        SQLiteDatabase db = mDatasource.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TracksContract.UID, ShortId.generate());
        values.put(TracksContract.STATE, TracksContract.STATE_IDLE);
        values.put(TracksContract.PROFILE_ID, profile.getId());
        return db.insert(TrackDatasource.TABLE_TRACK, null, values);
    }

    private void createNotification(long trackId, Profile profile) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setColor(getResources().getColor(R.color.app_primary));
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setContentTitle(getResources().getString(R.string.track_notification_title));
        builder.setContentText(profile.getName());
        builder.setOngoing(true);
        builder.setAutoCancel(false);

        Intent resultIntent = new Intent(this, TrackActivity.class);
        resultIntent.putExtra(TrackActivity.EXTRA_TRACK_ID, trackId);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) trackId, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        mNotificationManager.notify((int) trackId, builder.build());
    }

    private void cancelNotification(long trackId) {
        mNotificationManager.cancel((int) trackId);
    }

}
