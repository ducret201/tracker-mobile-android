package com.livepeak.mobile.track;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.livepeak.common.Distance;
import com.livepeak.common.Duration;
import com.livepeak.mobile.R;

/**
 * Created by guillaume on 20/05/15.
 */
public class MetricView extends RelativeLayout {

    public static final int TYPE_DURATION = 0;

    public static final int TYPE_DISTANCE = 1;

    public static final int TYPE_ALTITUDE = 2;

    public static final int TYPE_BEARING = 3;

    private TextView mLabelText;

    private TextView mValueText;

    private TextView mUnitText;

    private int mType;

    public MetricView(Context context) {
        super(context);
        initMetricView(null);
    }

    public MetricView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initMetricView(attrs);
    }

    public MetricView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initMetricView(attrs);
    }

    private void initMetricView(AttributeSet attrs) {
        inflate(getContext(), R.layout.track_metric_view, this);

        mLabelText = (TextView) findViewById(R.id.label_text);
        mValueText = (TextView) findViewById(R.id.value_text);
        mUnitText = (TextView) findViewById(R.id.unit_text);

        mLabelText.setText("");
        mValueText.setText("------");
        mUnitText.setText("");

        if (attrs != null) {
            TypedArray styledAttrs = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.MetricView, 0, 0);
            try {
                setType(styledAttrs.getInteger(R.styleable.MetricView_metric_type, TYPE_DISTANCE));
            } finally {
                styledAttrs.recycle();
            }
        }
    }

    public void setType(int type) {
        mType = type;

        int labelResourceId = -1;
        switch (mType) {
            case TYPE_DURATION:
                labelResourceId = R.string.track_metric_duration_label;
                break;
            case TYPE_DISTANCE:
                labelResourceId = R.string.track_metric_distance_label;
                break;
            case TYPE_ALTITUDE:
                labelResourceId = R.string.track_metric_altitude_label;
                break;
            case TYPE_BEARING:
                labelResourceId = R.string.track_metric_bearing_label;
                break;
        }

        mLabelText.setText(labelResourceId);
    }

    public void setValue(double value) {
        String[] formatted = new String[2];
        switch (mType) {
            case TYPE_DURATION:
                (new Duration((long) value)).format(formatted);
                break;
            case TYPE_DISTANCE:
                (new Distance((long) value)).format(formatted);
                break;
            case TYPE_ALTITUDE:
                (new Distance((long) value)).format(Distance.UNIT_M, formatted);
                break;
            case TYPE_BEARING:
                formatted[0] = Long.toString((long) value);
                formatted[1] = "degree";
                break;
        }

        mValueText.setText(formatted[0]);
        mUnitText.setText(formatted[1]);
    }
}
