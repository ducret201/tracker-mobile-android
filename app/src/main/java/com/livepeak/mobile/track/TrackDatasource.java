package com.livepeak.mobile.track;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.livepeak.android.database.CursorHolder;

/**
 * Created by guillaume on 12/01/15.
 */
public class TrackDatasource extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "track.db";

    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_TRACK = "track";

    public static final String TABLE_POSITION = "position";

    public static final String COLUMN_ID = "_id";

    public static final String CREATE_TABLE_TRACK = "CREATE TABLE " + TABLE_TRACK + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            TracksContract.UID + " TEXT NOT NULL," +
            TracksContract.DURATION + " INTEGER," +
            TracksContract.DISTANCE + " INTEGER," +
            TracksContract.STATE + " TEXT NOT NULL," +
            TracksContract.STATUS + " INTEGER DEFAULT 0 NOT NULL," +
            TracksContract.PROFILE_ID + " INTEGER NOT NULL" +
            ")";

    public static final String CREATE_TABLE_POSITION = "CREATE TABLE " + TABLE_POSITION + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            TracksContract.Position.LAT + " NUMERIC NOT NULL," +
            TracksContract.Position.LNG + " NUMERIC NOT NULL," +
            TracksContract.Position.ALTITUDE + " INTEGER," +
            TracksContract.Position.BEARING + " INTEGER," +
            TracksContract.Position.TIMESTAMP + " INTEGER NOT NULL," +
            TracksContract.Position.STATUS + " INTEGER DEFAULT 0 NOT NULL," +
            TracksContract.Position.FLAG + " TEXT," +
            TracksContract.Position.TRACK_ID + " INTEGER REFERENCES " + TABLE_TRACK + "(" + COLUMN_ID + ") NOT NULL" +
            ")";

    public TrackDatasource(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TRACK);
        db.execSQL(CREATE_TABLE_POSITION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public static class TrackCursorHolder extends CursorHolder {

        public final int uidIndex;

        public final int durationIndex;

        public final int distanceIndex;

        public final int stateIndex;

        public final int statusIndex;

        public final int profileIdIndex;

        public TrackCursorHolder(Cursor cursor) {
            super(cursor);

            this.uidIndex = cursor.getColumnIndex(TracksContract.UID);
            this.durationIndex = cursor.getColumnIndex(TracksContract.DURATION);
            this.distanceIndex = cursor.getColumnIndex(TracksContract.DISTANCE);
            this.stateIndex = cursor.getColumnIndex(TracksContract.STATE);
            this.statusIndex = cursor.getColumnIndex(TracksContract.STATUS);
            this.profileIdIndex = cursor.getColumnIndex(TracksContract.PROFILE_ID);
        }
    }

    public static class PositionCursorHolder extends CursorHolder {

        public final int latIndex;

        public final int lngIndex;

        public final int altitudeIndex;

        public final int bearingIndex;

        public final int timestampIndex;

        public final int statusIndex;

        public final int flagIndex;

        public final int trackIdIndex;

        public PositionCursorHolder(Cursor cursor) {
            super(cursor);

            this.latIndex = cursor.getColumnIndex(TracksContract.Position.LAT);
            this.lngIndex = cursor.getColumnIndex(TracksContract.Position.LNG);
            this.altitudeIndex = cursor.getColumnIndex(TracksContract.Position.ALTITUDE);
            this.bearingIndex = cursor.getColumnIndex(TracksContract.Position.BEARING);
            this.timestampIndex = cursor.getColumnIndex(TracksContract.Position.TIMESTAMP);
            this.statusIndex = cursor.getColumnIndex(TracksContract.Position.STATUS);
            this.flagIndex = cursor.getColumnIndex(TracksContract.Position.FLAG);
            this.trackIdIndex = cursor.getColumnIndex(TracksContract.Position.TRACK_ID);
        }
    }
}
