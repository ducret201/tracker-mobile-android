package com.livepeak.mobile.track;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.livepeak.common.Moment;
import com.livepeak.mobile.R;


/**
 * Created by guillaume on 20/05/15.
 */
public class StateView extends RelativeLayout {

    private ProgressBar mStateProgress;

    private ImageView mStateImage;

    private TextView mStatusText;

    private String mState;

    private int mFrequency = -1;

    private long mLastSentPositionTimestamp = -1;

    public StateView(Context context) {
        super(context);
        init();
    }

    public StateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StateView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.track_state_view, this);

        mStateProgress = (ProgressBar) findViewById(R.id.state_progress);
        mStateProgress.setVisibility(View.INVISIBLE);
        mStateImage = (ImageView) findViewById(R.id.state_image);
        mStateImage.setVisibility(View.INVISIBLE);
        mStatusText = (TextView) findViewById(R.id.status_text);
    }

    public void setState(String state) {
        mState = state;
        setState();
        setStatus();
    }

    private void setState() {
        if (TracksContract.STATE_IDLE.equals(mState)) {
            mStateProgress.setVisibility(View.VISIBLE);
            mStateImage.setVisibility(View.INVISIBLE);
        } else if (TracksContract.STATE_RUNNING.equals(mState)) {
            mStateProgress.setVisibility(View.INVISIBLE);
            mStateImage.setVisibility(View.VISIBLE);
            mStateImage.getDrawable().mutate().setColorFilter(getResources().getColor(R.color.success), PorterDuff.Mode.MULTIPLY);
        } else if (TracksContract.STATE_LOST.equals(mState)) {
            mStateProgress.setVisibility(View.INVISIBLE);
            mStateImage.setVisibility(View.VISIBLE);
            mStateImage.getDrawable().mutate().setColorFilter(getResources().getColor(R.color.error), PorterDuff.Mode.MULTIPLY);
        } else {
            mStateProgress.setVisibility(View.INVISIBLE);
            mStateImage.setVisibility(View.VISIBLE);
            mStateImage.getDrawable().mutate().setColorFilter(null);
        }
    }

    private void setStatus() {
        if (TracksContract.STATE_IDLE.equals(mState)) {
            mStatusText.setText(getResources().getString(R.string.track_status_idle));
        } else if (TracksContract.STATE_RUNNING.equals(mState) && mLastSentPositionTimestamp >= 0) {
            mStatusText.setText(getResources().getString(R.string.track_status_running) + " " + new Moment(mLastSentPositionTimestamp).fromNow());
        } else if (TracksContract.STATE_LOST.equals(mState) && mLastSentPositionTimestamp >= 0) {
            mStatusText.setText(getResources().getString(R.string.track_status_lost) + " " + new Moment(mLastSentPositionTimestamp).fromNow());
        } else if (TracksContract.STATE_TERMINATED.equals(mState)) {
            mStatusText.setText(getResources().getString(R.string.track_status_terminated));
        }
    }

    public void setActivity(String activity) {
        mStateImage.setImageResource(getActivityResourceId(activity));
    }

    public void setFrequency(int frequency) {
        mFrequency = frequency;
        mState = computeState();
        setState();
        setStatus();
    }

    public void setLastSentPositionTimestamp(long timestamp) {
        mLastSentPositionTimestamp = timestamp;
        mState = computeState();
        setState();
        setStatus();
    }

    public void refresh() {
        mState = computeState();
        setState();
        setStatus();
    }

    private int getActivityResourceId(String activityName) {
        if (activityName == null) return getDefaultResourceId();

        String name = "ic_activity_" + activityName.replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + "_large";
        Resources resources = getContext().getResources();
        return resources.getIdentifier(name, "drawable", getContext().getPackageName());
    }

    private String computeState() {
        if (mLastSentPositionTimestamp < 0 || mFrequency < 0) return mState;

        String state;
        long delay = (System.currentTimeMillis() - mLastSentPositionTimestamp) / 1000; // seconds

        if (delay > getToleranceFrequency(mFrequency)) {
            state = TracksContract.STATE_LOST;
        } else {
            state = TracksContract.STATE_RUNNING;
        }

        return state;
    }

    private static int getToleranceFrequency(int frequency) {
        if (frequency < 10) return frequency * 2;
        if (frequency < 60) return (int) (frequency * 1.5);
        return (int) (frequency * 1.1);
    }

    private static int getDefaultResourceId() {
        return R.drawable.ic_activity_walking_large;
    }
}
