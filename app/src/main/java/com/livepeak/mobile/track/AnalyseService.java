package com.livepeak.mobile.track;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.livepeak.mobile.app.TrackerApplication;
import com.livepeak.mobile.profile.ProfileDatasource;
import com.livepeak.mobile.profile.ProfilesContract;

/**
 * Created by guillaume on 07/07/15.
 */
public class AnalyseService extends IntentService {

    private static final String TAG = AnalyseService.class.getSimpleName();

    public final static String BROADCAST_ACTION_ANALYSED = AnalyseService.class.getPackage().getName() + ".intent.action.ANALYSED";

    public static final String EXTRA_TRACK_ID = "TRACK_ID";

    public static final String EXTRA_ANALYSE = "ANALYSE";

    public static final int ANALYSE_METRICS = 10;

    public static final int ANALYSE_STATE = 20;

    private final static String WHERE_TRACK = TrackDatasource.COLUMN_ID + " = ?";

    private final static String SELECTION_PROFILE = "_id = ?";

    private final static String SELECTION_TRACK = "_id = ?";

    private final static String SELECTION_POSITIONS = TracksContract.Position.TRACK_ID + " = ?";

    private final static String SELECTION_POSITIONS_SENT = TracksContract.Position.TRACK_ID + " = ? AND (" + TracksContract.Position.STATUS + " & ?) > 0";

    private TrackDatasource mTrackDatasource;

    private ProfileDatasource mProfileDatasource;

    public AnalyseService() {
        super(AnalyseService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mTrackDatasource = ((TrackerApplication) getApplication()).getTrackDatasource();
        mProfileDatasource = ((TrackerApplication) getApplication()).getProfileDatasource();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long trackId = intent.getLongExtra(EXTRA_TRACK_ID, -1);
        int analyse = intent.getIntExtra(EXTRA_ANALYSE, -1);
        if (trackId < 0) return;

        switch (analyse) {
            case ANALYSE_METRICS:
                analyseMetrics(trackId);
                Log.d(TAG, "Track {_id: " + trackId + "} metrics analysed");
                break;
            case ANALYSE_STATE:
                analyseState(trackId);
                Log.d(TAG, "Track {_id: " + trackId + "} state analysed");
                break;
        }

        // broadcast
        Intent broadcastIntent = new Intent(BROADCAST_ACTION_ANALYSED);
        broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private void analyseMetrics(long trackId) {
        SQLiteDatabase db = mTrackDatasource.getWritableDatabase();
        Cursor beginPositionCursor = null;
        Cursor endPositionCursor = null;

        try {
            db.beginTransaction();

            // query begin position
            String[] selectionArgs = {Long.toString(trackId)};
            beginPositionCursor = db.query(TrackDatasource.TABLE_POSITION, TracksContract.Position.PROJECTION, SELECTION_POSITIONS, selectionArgs, null, null, "_id ASC", "1");
            TrackDatasource.PositionCursorHolder beginPositionCursorHolder = new TrackDatasource.PositionCursorHolder(beginPositionCursor);

            // query end position
            endPositionCursor = db.query(TrackDatasource.TABLE_POSITION, TracksContract.Position.PROJECTION, SELECTION_POSITIONS, selectionArgs, null, null, "_id DESC", "1");
            TrackDatasource.PositionCursorHolder endPositionCursorHolder = new TrackDatasource.PositionCursorHolder(endPositionCursor);

            if (beginPositionCursor.moveToFirst() && endPositionCursor.moveToFirst()) {
                // update track
                String[] whereArgs = {Long.toString(trackId)};
                ContentValues values = new ContentValues();
                values.put(TracksContract.DURATION, computeTrackDuration(beginPositionCursorHolder, endPositionCursorHolder));
                values.put(TracksContract.DISTANCE, computeTrackDistance(beginPositionCursorHolder, endPositionCursorHolder));
                db.update(TrackDatasource.TABLE_TRACK, values, WHERE_TRACK, whereArgs);

                // notify content resolver
                getContentResolver().notifyChange(TracksContract.CONTENT_URI.buildUpon().appendPath(Long.toString(trackId)).build(), null);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            if (beginPositionCursor != null) beginPositionCursor.close();
            if (endPositionCursor != null) endPositionCursor.close();
        }
    }

    private void analyseState(long trackId) {
        SQLiteDatabase db = mTrackDatasource.getWritableDatabase();
        Cursor trackCursor = null;
        Cursor lastSentPositionCursor = null;

        try {
            db.beginTransaction();

            // query track
            String[] selectionArgs = {Long.toString(trackId)};
            trackCursor = db.query(TrackDatasource.TABLE_TRACK, TracksContract.PROJECTION, SELECTION_TRACK, selectionArgs, null, null, null);
            TrackDatasource.TrackCursorHolder trackCursorHolder = new TrackDatasource.TrackCursorHolder(trackCursor);

            if (trackCursor.moveToFirst()) {

                // query last position sent
                selectionArgs = new String[]{Long.toString(trackId), Integer.toString(TracksContract.STATUS_SENT)};
                lastSentPositionCursor = db.query(TrackDatasource.TABLE_POSITION, TracksContract.Position.PROJECTION, SELECTION_POSITIONS_SENT, selectionArgs, null, null, "_id DESC", "1");
                TrackDatasource.PositionCursorHolder lastSentPositionCursorHolder = new TrackDatasource.PositionCursorHolder(lastSentPositionCursor);

                // update track
                String[] whereArgs = {Long.toString(trackId)};
                ContentValues values = new ContentValues();
                values.put(TracksContract.STATE, computeTrackState(lastSentPositionCursorHolder, queryFrequency(trackCursor.getLong(trackCursorHolder.profileIdIndex))));
                db.update(TrackDatasource.TABLE_TRACK, values, WHERE_TRACK, whereArgs);

                // notify content resolver
                getContentResolver().notifyChange(TracksContract.CONTENT_URI.buildUpon().appendPath(Long.toString(trackId)).build(), null);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            if (trackCursor != null) trackCursor.close();
            if (lastSentPositionCursor != null) lastSentPositionCursor.close();
        }
    }

    private int queryFrequency(long profileId) {
        SQLiteDatabase db = mProfileDatasource.getReadableDatabase();
        Cursor profileCursor = null;

        int frequency = 0;
        try {
            // query profile
            String[] selectionArgs = {Long.toString(profileId)};
            profileCursor = db.query(ProfileDatasource.TABLE_PROFILE, ProfilesContract.PROJECTION, SELECTION_PROFILE, selectionArgs, null, null, null);
            ProfileDatasource.ProfileCursorHolder profileCursorHolder = new ProfileDatasource.ProfileCursorHolder(profileCursor);

            if (profileCursor.moveToFirst()) {
                frequency = profileCursor.getInt(profileCursorHolder.frequencyIndex);
            }
        } finally {
            if (profileCursor != null) profileCursor.close();
        }
        return frequency;
    }

    private long computeTrackDuration(TrackDatasource.PositionCursorHolder beginCursorHolder, TrackDatasource.PositionCursorHolder endCursorHolder) {
        long startTimeStamp = beginCursorHolder.getCursor().getLong(beginCursorHolder.timestampIndex);
        long endTimeStamp = endCursorHolder.getCursor().getLong(beginCursorHolder.timestampIndex);

        return endTimeStamp - startTimeStamp;
    }

    private long computeTrackDistance(TrackDatasource.PositionCursorHolder beginCursorHolder, TrackDatasource.PositionCursorHolder endCursorHolder) {
        double startLat = beginCursorHolder.getCursor().getDouble(beginCursorHolder.latIndex);
        double startLng = beginCursorHolder.getCursor().getDouble(beginCursorHolder.lngIndex);
        double endLat = endCursorHolder.getCursor().getDouble(beginCursorHolder.latIndex);
        double endLng = endCursorHolder.getCursor().getDouble(beginCursorHolder.lngIndex);

        float[] distance = new float[1];
        Location.distanceBetween(startLat, startLng, endLat, endLng, distance);
        return (long) distance[0];
    }

    private String computeTrackState(TrackDatasource.PositionCursorHolder lastSentPositionCursorHolder, int frequency) {
        SharedPreferences prefs = getSharedPreferences(TrackerApplication.APP_PREFS, MODE_PRIVATE);
        Cursor cursor = lastSentPositionCursorHolder.getCursor();

        String state;
        if (cursor.moveToFirst()) {
            String flag = cursor.getString(lastSentPositionCursorHolder.flagIndex);
            long timestamp = cursor.getLong(lastSentPositionCursorHolder.timestampIndex) + prefs.getLong(TrackerApplication.PREFS_GPS_TIME_DRIFT, 0);
            long delay = (System.currentTimeMillis() - timestamp) / 1000; // seconds

            if (TracksContract.Position.FLAG_STOP.equals(flag)) {
                state = TracksContract.STATE_TERMINATED;
            } else if (delay > getToleranceFrequency(frequency)) {
                state = TracksContract.STATE_LOST;
            } else {
                state = TracksContract.STATE_RUNNING;
            }

        } else {
            state = TracksContract.STATE_IDLE;
        }
        return state;
    }

    private static int getToleranceFrequency(int frequency) {
        if (frequency < 10) return frequency * 2;
        if (frequency < 60) return (int) (frequency * 1.5);
        return (int) (frequency * 1.1);
    }

    public static class Receiver extends BroadcastReceiver {

        private int mAnalyse;

        public Receiver(int analyse) {
            mAnalyse = analyse;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent serviceIntent = new Intent(context, AnalyseService.class);
            serviceIntent.putExtra(EXTRA_TRACK_ID, intent.getLongExtra(EXTRA_TRACK_ID, -1));
            serviceIntent.putExtra(EXTRA_ANALYSE, mAnalyse);
            context.startService(serviceIntent);
        }
    }
}
