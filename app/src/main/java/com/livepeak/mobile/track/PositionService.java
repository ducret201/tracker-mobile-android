package com.livepeak.mobile.track;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.SparseArray;

import com.livepeak.mobile.app.TrackerApplication;

/**
 * Created by guillaume on 11/01/15.
 */
public class PositionService extends IntentService {

    private static final String TAG = PositionService.class.getSimpleName();

    public final static String BROADCAST_ACTION_RECEIVED = PositionService.class.getPackage().getName() + ".intent.action.RECEIVED";

    public static final String EXTRA_TRACK_ID = "TRACK_ID";

    public static final String EXTRA_POSITION_ID = "POSITION_ID";

    public static final String EXTRA_FLAG = "FLAG";

    private static final long MIN_DISPLACEMENT_M = 15;

    private static SparseArray<Location> sLocationCache = new SparseArray<>();

    private static long sGpsTimeDrift;

    private TrackDatasource mDatasource;

    public PositionService() {
        super(PositionService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDatasource = ((TrackerApplication) getApplication()).getTrackDatasource();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDatasource = null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long trackId = intent.getLongExtra(EXTRA_TRACK_ID, -1);
        String flag = intent.getStringExtra(EXTRA_FLAG);
        Location newLocation = intent.getParcelableExtra(LocationManager.KEY_LOCATION_CHANGED);

        Location location = getValidLocation(newLocation, sLocationCache.get((int) trackId));
        if (trackId >= 0 && location != null) {
            sLocationCache.put((int) trackId, location);

            saveGpsTimeDrift(location);
            long positionId = savePosition(trackId, location, flag);
            Log.d(TAG, "Position saved for track {_id: " + trackId + "}");

            // broadcast
            Intent broadcastIntent = new Intent(BROADCAST_ACTION_RECEIVED);
            broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
            broadcastIntent.putExtra(EXTRA_POSITION_ID, positionId);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        }
    }

    private Location getValidLocation(Location newLocation, Location oldLocation) {
        if (newLocation == null) return null;

        Log.d(TAG, "Position time " + newLocation.getTime());
        Log.d(TAG, "Position provider " + newLocation.getProvider());
        Log.d(TAG, "Position accuracy " + newLocation.getAccuracy());
        Log.d(TAG, "Position latLng " + newLocation.getLatitude() + ',' + newLocation.getLongitude());

        if (oldLocation != null && newLocation.distanceTo(oldLocation) < MIN_DISPLACEMENT_M) {
            Log.d(TAG, "Location didn't move. Keep old location");
            oldLocation.setTime(newLocation.getTime());
            return oldLocation;
        }

        return newLocation;
    }

    private void saveGpsTimeDrift(Location location) {
        long drift = Math.round((System.currentTimeMillis() - location.getTime()) / 1000) * 1000;
        if (sGpsTimeDrift != drift) {
            sGpsTimeDrift = drift;

            SharedPreferences.Editor editPrefs = getSharedPreferences(TrackerApplication.APP_PREFS, MODE_PRIVATE).edit();
            editPrefs.putLong(TrackerApplication.PREFS_GPS_TIME_DRIFT, drift);
            editPrefs.apply();
        }
    }

    private long savePosition(long trackId, Location location, String flag) {
        long result;

        SQLiteDatabase db = mDatasource.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TracksContract.Position.LAT, location.getLatitude());
        values.put(TracksContract.Position.LNG, location.getLongitude());
        values.put(TracksContract.Position.TIMESTAMP, location.getTime());
        values.put(TracksContract.Position.FLAG, flag);
        values.put(TracksContract.Position.TRACK_ID, trackId);

        if (location.hasAltitude()) {
            values.put(TracksContract.Position.ALTITUDE, (long) location.getAltitude());
        }
        if (location.hasBearing()) {
            values.put(TracksContract.Position.BEARING, (long) location.getBearing());
        }

        result =  db.insert(TrackDatasource.TABLE_POSITION, null, values);

        // notify content resolver
        getContentResolver().notifyChange(TracksContract.Position.CONTENT_URI, null);

        return result;
    }
}
