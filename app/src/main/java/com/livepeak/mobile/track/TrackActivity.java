package com.livepeak.mobile.track;

import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.livepeak.android.app.LocationProviderDelegate;
import com.livepeak.mobile.R;
import com.livepeak.mobile.app.TrackerApplication;
import com.livepeak.mobile.profile.Profile;
import com.livepeak.mobile.profile.ProfileDatasource;
import com.livepeak.mobile.profile.ProfilesContract;

public class TrackActivity extends ActionBarActivity implements LocationProviderDelegate.Listener {

    private static final String TAG = TrackActivity.class.getSimpleName();

    private static final int LOADER_PROFILE = 10;

    private static final int LOADER_TRACK = 20;

    private static final int LOADER_POSITION_FIRST = 30;

    private static final int LOADER_POSITION_LAST = 40;

    private static final int LOADER_POSITION_LAST_SENT = 50;

    public static final String EXTRA_PROFILE = "profile";

    public static final String EXTRA_TRACK_ID = "TRACK_ID";

    private static final String ARGS_TRACK_ID = "TRACK_ID";

    private static final String ARGS_PROFILE_ID = "PROFILE_ID";

    private final static String SELECTION_POSITION = TracksContract.Position.TRACK_ID + " = ?";

    private final static String SELECTION_POSITION_SENT = TracksContract.Position.TRACK_ID + " = ? AND (" + TracksContract.STATUS + " & ?) > 0";

    private final static long BACK_PRESSED_DELAY = 2000;

    private final static Uri TRACK_BASE_URI = Uri.parse("http://www.golivepeak.com");

    private final LocationProviderDelegate mLocationProviderDelegate;

    private ProfileLoaderCallbacks mProfileLoaderCallbacks;

    private TrackLoaderCallbacks mTrackLoaderCallbacks;

    private PositionLoaderCallbacks mPositionLoaderCallbacks;

    private Receiver mReceiver;

    private StateView mStatus;

    private DurationMetricView mDurationMetric;

    private MetricView mDistanceMetric;

    private MetricView mAltitudeMetric;

    private MetricView mBearingMetric;

    private long mTrackId = -1;

    private String mTrackUid;

    private long mBackPressedTimestamp;

    private Toast mBackPressedToast;

    private boolean mViewTrackActionVisible = false;

    public TrackActivity() {
        mLocationProviderDelegate = new LocationProviderDelegate(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationProviderDelegate.onCreate();
        setContentView(R.layout.track_activity);

        mProfileLoaderCallbacks = new ProfileLoaderCallbacks();
        mTrackLoaderCallbacks = new TrackLoaderCallbacks();
        mPositionLoaderCallbacks = new PositionLoaderCallbacks();

        mReceiver = new Receiver();

        mStatus = (StateView) findViewById(R.id.status);
        mDurationMetric = (DurationMetricView) findViewById(R.id.duration_metric);
        mDistanceMetric = (MetricView) findViewById(R.id.distance_metric);
        mAltitudeMetric = (MetricView) findViewById(R.id.altitude_metric);
        mBearingMetric = (MetricView) findViewById(R.id.bearing_metric);

        Intent intent = getIntent();
        Profile profile = intent.getParcelableExtra(EXTRA_PROFILE);
        mTrackId = intent.getLongExtra(EXTRA_TRACK_ID, -1);

        if (mTrackId < 0  && savedInstanceState != null) {
            mTrackId = savedInstanceState.getLong(EXTRA_TRACK_ID);
        }

        if (mTrackId < 0) {
            // start new tracking
            Intent serviceIntent = new Intent(this, TrackService.class);
            serviceIntent.putExtra(TrackService.EXTRA_PROFILE, profile);
            startService(serviceIntent);
        } else {
            // load track
            Bundle args = new Bundle();
            args.putLong(ARGS_TRACK_ID, mTrackId);
            getLoaderManager().initLoader(LOADER_TRACK, args, mTrackLoaderCallbacks);

            // load first position
            args = new Bundle();
            args.putLong(ARGS_TRACK_ID, mTrackId);
            getLoaderManager().initLoader(LOADER_POSITION_FIRST, args, mPositionLoaderCallbacks);

            // load latest position
            args = new Bundle();
            args.putLong(ARGS_TRACK_ID, mTrackId);
            getLoaderManager().initLoader(LOADER_POSITION_LAST, args, mPositionLoaderCallbacks);

            // load latest sent position
            args = new Bundle();
            args.putLong(ARGS_TRACK_ID, mTrackId);
            getLoaderManager().initLoader(LOADER_POSITION_LAST_SENT, args, mPositionLoaderCallbacks);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocationProviderDelegate.onResume();

        // refresh timed metrics / status
        mStatus.refresh();
        mDurationMetric.refresh();

        IntentFilter filter = new IntentFilter();
        filter.addAction(TrackService.BROADCAST_ACTION_STARTED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Profile profile = intent.getParcelableExtra(EXTRA_PROFILE);
        long trackId = intent.getLongExtra(EXTRA_TRACK_ID, -1);

        if (mTrackId != trackId) {
            mTrackId = trackId;

            if (mTrackId < 0) {
                // start new tracking
                Intent serviceIntent = new Intent(this, TrackService.class);
                serviceIntent.putExtra(TrackService.EXTRA_PROFILE, profile);
                startService(serviceIntent);
            } else {
                // make sure profile loader is restarted
                getLoaderManager().destroyLoader(LOADER_PROFILE);

                // load track
                Bundle args = new Bundle();
                args.putLong(ARGS_TRACK_ID, mTrackId);
                getLoaderManager().restartLoader(LOADER_TRACK, args, mTrackLoaderCallbacks);

                // load first position
                args = new Bundle();
                args.putLong(ARGS_TRACK_ID, mTrackId);
                getLoaderManager().restartLoader(LOADER_POSITION_FIRST, args, mPositionLoaderCallbacks);

                // load latest position
                args = new Bundle();
                args.putLong(ARGS_TRACK_ID, mTrackId);
                getLoaderManager().restartLoader(LOADER_POSITION_LAST, args, mPositionLoaderCallbacks);

                // load latest sent position
                args = new Bundle();
                args.putLong(ARGS_TRACK_ID, mTrackId);
                getLoaderManager().restartLoader(LOADER_POSITION_LAST_SENT, args, mPositionLoaderCallbacks);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.track_menu, menu);

        MenuItem item = menu.findItem(R.id.action_view_track);
        item.setVisible(mViewTrackActionVisible);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_track:
                Uri uri = TRACK_BASE_URI.buildUpon().appendPath(mTrackUid).build();
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putLong(EXTRA_TRACK_ID, mTrackId);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (System.currentTimeMillis() - mBackPressedTimestamp < BACK_PRESSED_DELAY) {
                    // back button pressed twice
                    mBackPressedTimestamp = 0;
                    mBackPressedToast.cancel();
                    mBackPressedToast = null;

                    if (mTrackId >= 0) {
                        // stop tracking
                        Intent serviceIntent = new Intent(this, TrackService.class);
                        serviceIntent.putExtra(TrackService.EXTRA_ACTION, TrackService.ACTION_STOP_TRACKING);
                        serviceIntent.putExtra(TrackService.EXTRA_TRACK_ID, mTrackId);
                        startService(serviceIntent);

                        finish();
                    }
                } else {
                    // back button pressed once
                    mBackPressedTimestamp = System.currentTimeMillis();
                    mBackPressedToast = Toast.makeText(this, "Tap back button again to exit", Toast.LENGTH_SHORT);
                    mBackPressedToast.show();
                }
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onLocationProviderDisabled() {
        // stop tracking
        Intent serviceIntent = new Intent(this, TrackService.class);
        serviceIntent.putExtra(TrackService.EXTRA_ACTION, TrackService.ACTION_STOP_TRACKING);
        serviceIntent.putExtra(TrackService.EXTRA_TRACK_ID, mTrackId);
        startService(serviceIntent);

        finish();
    }

    private void setProfile(ProfileDatasource.ProfileCursorHolder cursorHolder) {
        Cursor cursor = cursorHolder.getCursor();
        mStatus.setActivity(cursor.getString(cursorHolder.activityIndex));
        mStatus.setFrequency(cursor.getInt(cursorHolder.frequencyIndex));
    }

    private void setTrack(TrackDatasource.TrackCursorHolder cursorHolder) {
        Cursor cursor = cursorHolder.getCursor();
        setState(cursor.getString(cursorHolder.stateIndex));
        mTrackUid = cursor.getString(cursorHolder.uidIndex);
        mStatus.setState(cursor.getString(cursorHolder.stateIndex));

        if (!cursor.isNull(cursorHolder.distanceIndex)) {
            mDistanceMetric.setValue(cursor.getLong(cursorHolder.distanceIndex));
        }
    }

    private void setFirstPosition(TrackDatasource.PositionCursorHolder cursorHolder) {
        SharedPreferences prefs = getSharedPreferences(TrackerApplication.APP_PREFS, MODE_PRIVATE);
        Cursor cursor = cursorHolder.getCursor();

        long timestamp = cursor.getLong(cursorHolder.timestampIndex) + prefs.getLong(TrackerApplication.PREFS_GPS_TIME_DRIFT, 0);
        mDurationMetric.setBeginTimestamp(timestamp);
    }

    private void setLastPosition(TrackDatasource.PositionCursorHolder cursorHolder) {
        Cursor cursor = cursorHolder.getCursor();

        if (!cursor.isNull(cursorHolder.altitudeIndex)) {
            mAltitudeMetric.setValue(cursor.getLong(cursorHolder.altitudeIndex));
        }
        if (!cursor.isNull(cursorHolder.bearingIndex)) {
            mBearingMetric.setValue(cursor.getFloat(cursorHolder.bearingIndex));
        }
    }

    private void setLastSentPosition(TrackDatasource.PositionCursorHolder cursorHolder) {
        SharedPreferences prefs = getSharedPreferences(TrackerApplication.APP_PREFS, MODE_PRIVATE);
        Cursor cursor = cursorHolder.getCursor();

        long timestamp = cursor.getLong(cursorHolder.timestampIndex) + prefs.getLong(TrackerApplication.PREFS_GPS_TIME_DRIFT, 0);
        mStatus.setLastSentPositionTimestamp(timestamp);
    }

    private void setState(String state) {
        boolean viewTrackActionVisible;

        if (TracksContract.STATE_IDLE.equals(state)) {
            viewTrackActionVisible = false;
        } else {
            viewTrackActionVisible = true;
        }

        if (mViewTrackActionVisible != viewTrackActionVisible) {
            mViewTrackActionVisible = viewTrackActionVisible;
            invalidateOptionsMenu();
        }
    }

    private class ProfileLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            Uri uri = ProfilesContract.CONTENT_URI.buildUpon().appendPath(Long.toString(args.getLong(ARGS_PROFILE_ID, -1))).build();
            CursorLoader cursorLoader = new CursorLoader(TrackActivity.this, uri, ProfilesContract.PROJECTION, null, null, null);
            return cursorLoader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if (cursor.moveToFirst()) {
                setProfile(new ProfileDatasource.ProfileCursorHolder(cursor));
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
        }
    }

    private class TrackLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {Log.d(TAG, "oncreateloader");
            Uri uri = TracksContract.CONTENT_URI.buildUpon().appendPath(Long.toString(args.getLong(ARGS_TRACK_ID, -1))).build();
            CursorLoader cursorLoader = new CursorLoader(TrackActivity.this, uri, TracksContract.PROJECTION, null, null, null);
            return cursorLoader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {Log.d(TAG, "onLoadFinished");
            if (cursor.moveToFirst()) {
                TrackDatasource.TrackCursorHolder cursorHolder = new TrackDatasource.TrackCursorHolder(cursor);
                setTrack(cursorHolder);

                // load profile
                Bundle args = new Bundle();
                args.putLong(ARGS_PROFILE_ID, cursor.getLong(cursorHolder.profileIdIndex));
                getLoaderManager().initLoader(LOADER_PROFILE, args, mProfileLoaderCallbacks);
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
        }
    }

    private class PositionLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            Loader<Cursor> loader = null;
            Uri uri;
            String[] selectionArgs;

            switch(id) {
                case LOADER_POSITION_FIRST:
                    uri = TracksContract.Position.CONTENT_URI.buildUpon().appendQueryParameter("LIMIT", "1").build();
                    selectionArgs = new String[]{Long.toString(args.getLong(ARGS_TRACK_ID, -1))};
                    loader = new CursorLoader(TrackActivity.this, uri, TracksContract.Position.PROJECTION, SELECTION_POSITION, selectionArgs, "_id ASC");
                    break;
                case LOADER_POSITION_LAST:
                    uri = TracksContract.Position.CONTENT_URI.buildUpon().appendQueryParameter("LIMIT", "1").build();
                    selectionArgs = new String[]{Long.toString(args.getLong(ARGS_TRACK_ID, -1))};
                    loader = new CursorLoader(TrackActivity.this, uri, TracksContract.Position.PROJECTION, SELECTION_POSITION, selectionArgs, "_id DESC");
                    break;
                case LOADER_POSITION_LAST_SENT:
                    uri = TracksContract.Position.CONTENT_URI.buildUpon().appendQueryParameter("LIMIT", "1").build();
                    selectionArgs = new String[]{Long.toString(args.getLong(ARGS_TRACK_ID, -1)), Integer.toString(TracksContract.Position.STATUS_SENT)};
                    loader = new CursorLoader(TrackActivity.this, uri, TracksContract.Position.PROJECTION, SELECTION_POSITION_SENT, selectionArgs, "_id DESC");
                    break;
            }

            return loader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            switch(loader.getId()) {
                case LOADER_POSITION_FIRST:
                    if (cursor.moveToFirst()) {
                        setFirstPosition(new TrackDatasource.PositionCursorHolder(cursor));
                    }
                    break;
                case LOADER_POSITION_LAST:
                    if (cursor.moveToFirst()) {
                        setLastPosition(new TrackDatasource.PositionCursorHolder(cursor));
                    }
                    break;
                case LOADER_POSITION_LAST_SENT:
                    if (cursor.moveToFirst()) {
                        setLastSentPosition(new TrackDatasource.PositionCursorHolder(cursor));
                    }
                    break;
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
        }
    }

    private class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (TrackService.BROADCAST_ACTION_STARTED.equals(intent.getAction())) {
                long trackId = intent.getLongExtra(TrackService.EXTRA_TRACK_ID, -1);
                if (mTrackId < 0) {
                    mTrackId = trackId;

                    // load track
                    Bundle args = new Bundle();
                    args.putLong(ARGS_TRACK_ID, mTrackId);
                    getLoaderManager().initLoader(LOADER_TRACK, args, mTrackLoaderCallbacks);

                    // load first position
                    args = new Bundle();
                    args.putLong(ARGS_TRACK_ID, mTrackId);
                    getLoaderManager().initLoader(LOADER_POSITION_FIRST, args, mPositionLoaderCallbacks);

                    // load latest position
                    args = new Bundle();
                    args.putLong(ARGS_TRACK_ID, mTrackId);
                    getLoaderManager().initLoader(LOADER_POSITION_LAST, args, mPositionLoaderCallbacks);

                    // load latest sent position
                    args = new Bundle();
                    args.putLong(ARGS_TRACK_ID, mTrackId);
                    getLoaderManager().initLoader(LOADER_POSITION_LAST_SENT, args, mPositionLoaderCallbacks);
                } else {
                    Log.w(TAG, "New Track {_id: " + trackId + "} started while Track {_id: " + mTrackId + "} is displayed");
                }
            }
        }
    }
}
