package com.livepeak.mobile.track;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.livepeak.android.net.DisconnectedNetworkException;
import com.livepeak.mobile.app.TrackerApplication;
import com.livepeak.mobile.profile.ProfileDatasource;
import com.livepeak.mobile.profile.ProfilesContract;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by guillaume on 11/01/15.
 */
public class GatewayService extends IntentService {

    private static final String TAG = GatewayService.class.getSimpleName();

    public final static String BROADCAST_ACTION_TRACK_SENT = TrackService.class.getPackage().getName() + ".intent.action.TRACK_SENT";

    public final static String BROADCAST_ACTION_POSITION_SENT = TrackService.class.getPackage().getName() + ".intent.action.POSITION_SENT";

    public final static String BROADCAST_ACTION_FAILED = TrackService.class.getPackage().getName() + ".intent.action.FAILED";

    public static final String EXTRA_TRACK_ID = "TRACK_ID";

    public static final String EXTRA_RECOVERY_COUNT = "RECOVERY_COUNT";

    private final static String SELECTION_TRACK_SENT = "_id = ? AND (" + TracksContract.STATUS + " & ?) > 0";

    private final static String SELECTION_TRACK_UNSENT = "_id = ? AND (" + TracksContract.STATUS + " & ?) = 0";

    private final static String SELECTION_POSITIONS_UNSENT = "(" + TracksContract.Position.STATUS + " & ?) = 0";

    private final static String SELECTION_TRACK_POSITIONS_UNSENT = TracksContract.Position.TRACK_ID + " = ? AND (" + TracksContract.Position.STATUS + " & ?) = 0";

    private final static String SELECTION_PROFILE = "_id = ?";

    private final static String SELECTION_CONTACT = ProfilesContract.Contact.PROFILE_ID + " = ? AND " + ProfilesContract.Contact.TYPE + " = ?";

    private final static String[] PROJECTION_TRACK_ID = {TracksContract.Position.TRACK_ID};

    private final static String UPDATE_POSITION_SENT = "UPDATE " + TrackDatasource.TABLE_POSITION + " SET " + TracksContract.Position.STATUS + " = " + TracksContract.Position.STATUS + " | ?";

    private final static String UPDATE_TRACK_SENT = "UPDATE " + TrackDatasource.TABLE_TRACK + " SET " + TracksContract.UID + " = ?, " + TracksContract.STATUS + " = " + TracksContract.STATUS + " | ?";

    private final static String ENDPOINT = "http://www.golivepeak.com";

    private final static String AUTHORIZATION_HEADER = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOiJMaXZlcGVha01vYmlsZUFuZHJvaWQifQ.xVb-BxpdcyDREsUDhC8s-I7d-5q6J88a3F5uaN9wsI0";

    private final static int CONNECT_TIMEOUT = 5000;

    private final static int READ_TIMEOUT = 5000;

    private final static int RECOVER_DELAY = 10000;

    private final static int MAX_RECOVER_COUNT = 3;

    private static final String GEOJSON_TRACK_TEMPLATE;

    private static final String GEOJSON_POSITION_TEMPLATE;

    private static final String JSON_SHARING_TEMPLATE;

    private static final String JSON_SHARING_EMAIL_TEMPLATE;

    private TrackDatasource mDatasource;

    private ProfileDatasource mProfileDatasource;

    private ConnectivityManager mConnectivityManager;

    private AlarmManager mAlarmManager;

    static {
        GEOJSON_TRACK_TEMPLATE = "{" +
                "    \"type\": \"Feature\"," +
                "    \"properties\": {" +
                "        \"name\": \"{{properties.name}}\"," +
                "        \"userName\": \"{{properties.userName}}\"," +
                "        \"activity\": \"{{properties.activity}}\"," +
                "        \"frequency\": {{properties.frequency}}," +
                "        \"sharing\": {{properties.sharing}}," +
                "        \"flag\": \"{{properties.flag}}\"" +
                "    }," +
                "    \"geometry\": {" +
                "        \"type\": \"LineString\"," +
                "        \"coordinates\": {{geometry.coordinates}}" +
                "    }" +
                "}";

        GEOJSON_POSITION_TEMPLATE = "{" +
                "    \"type\": \"Feature\"," +
                "    \"properties\": {" +
                "        \"flag\": \"{{properties.flag}}\"" +
                "    }," +
                "    \"geometry\": {" +
                "        \"type\": \"MultiPoint\"," +
                "        \"coordinates\": {{geometry.coordinates}}" +
                "    }" +
                "}";

        JSON_SHARING_TEMPLATE = "{" +
                "    \"message\": \"{{message}}\"," +
                "    \"email\": {{email}}" +
                "}";

        JSON_SHARING_EMAIL_TEMPLATE = "{" +
                "    \"recipients\": {{recipients}}" +
                "}";
    }

    public GatewayService() {
        super(GatewayService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDatasource = ((TrackerApplication) getApplication()).getTrackDatasource();
        mProfileDatasource = ((TrackerApplication) getApplication()).getProfileDatasource();
        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long trackId = intent.getLongExtra(EXTRA_TRACK_ID, -1);
        int recoveryCount = intent.getIntExtra(EXTRA_RECOVERY_COUNT, 0);
        if (trackId < 0) {
            startServiceForTracks();
            return;
        }

        try {
            if (sendTrack(trackId)) {
                Log.d(TAG, "Track {_id: " + trackId + "} sent");

                // broadcast
                Intent broadcastIntent = new Intent(BROADCAST_ACTION_TRACK_SENT);
                broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            } else if (sendPositions(trackId)) {
                Log.d(TAG, "Position sent for track {_id: " + trackId + "}");

                // broadcast
                Intent broadcastIntent = new Intent(BROADCAST_ACTION_POSITION_SENT);
                broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
        } catch(DisconnectedNetworkException e) {
            Log.i(TAG, "No network available to send track {_id: " + trackId + "}");
            enableConnectivityReceiver();

            // broadcast
            Intent broadcastIntent = new Intent(BROADCAST_ACTION_FAILED);
            broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        } catch (IOException e) {
            Log.e(TAG, "Failed to send track {_id: " + trackId + "}", e);
            scheduleRecovery(trackId, recoveryCount);

            // broadcast
            Intent broadcastIntent = new Intent(BROADCAST_ACTION_FAILED);
            broadcastIntent.putExtra(EXTRA_TRACK_ID, trackId);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        }
    }

    public void startServiceForTracks() {
        SQLiteDatabase db = mDatasource.getReadableDatabase();
        Cursor cursor = null;

        try {
            // query trackIds which have unsent positions
            String[] selectionArgs = {Integer.toString(TracksContract.STATUS_SENT)};
            cursor = db.query(true, TrackDatasource.TABLE_POSITION, PROJECTION_TRACK_ID, SELECTION_POSITIONS_UNSENT, selectionArgs, null, null, null, null);
            TrackDatasource.PositionCursorHolder cursorHolder = new TrackDatasource.PositionCursorHolder(cursor);

            while (cursor.moveToNext()) {
                // start service
                Intent serviceIntent = new Intent(this, GatewayService.class);
                serviceIntent.putExtra(EXTRA_TRACK_ID, cursor.getLong(cursorHolder.trackIdIndex));
                startService(serviceIntent);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    private boolean sendTrack(long trackId) throws IOException{
        boolean result = false;

        SQLiteDatabase db = mDatasource.getWritableDatabase();
        Cursor trackCursor = null;
        Cursor positionCursor = null;

        try {
            db.beginTransaction();

            // query track
            String[] selectionArgs = {Long.toString(trackId), Integer.toString(TracksContract.STATUS_SENT)};
            trackCursor = db.query(TrackDatasource.TABLE_TRACK, TracksContract.PROJECTION, SELECTION_TRACK_UNSENT, selectionArgs, null, null, null);
            TrackDatasource.TrackCursorHolder trackCursorHolder = new TrackDatasource.TrackCursorHolder(trackCursor);

            if (trackCursor.moveToFirst()) {
                // track has not been sent yet
                // query positions
                selectionArgs = new String[]{Long.toString(trackId), Integer.toString(TracksContract.Position.STATUS_SENT)};
                positionCursor = db.query(TrackDatasource.TABLE_POSITION, TracksContract.Position.PROJECTION, SELECTION_TRACK_POSITIONS_UNSENT, selectionArgs, null, null, null);
                TrackDatasource.PositionCursorHolder positionCursorHolder = new TrackDatasource.PositionCursorHolder(positionCursor);

                if (positionCursor.moveToFirst()) {
                    Log.d(TAG, "Sending track {_id: " + trackId + "}");

                    // send track
                    String trackUid = trackCursor.getString(trackCursorHolder.uidIndex);
                    String geoJson = loadGeoJsonGeometry(GEOJSON_TRACK_TEMPLATE, positionCursorHolder);
                    geoJson = loadGeoJsonProperties(geoJson, positionCursorHolder);
                    geoJson = loadGeoJsonProperties(geoJson, trackCursor.getLong(trackCursorHolder.profileIdIndex));
                    send("/api/v1/track/" + trackUid, "PUT", cleanJson(geoJson));

                    // perform sql updates last so the RESERVED lock is held the least possible time
                    // update positions
                    String[] bindArgs = {Integer.toString(TracksContract.STATUS_SENT), Long.toString(trackId), Integer.toString(TracksContract.Position.STATUS_SENT)};
                    db.execSQL(UPDATE_POSITION_SENT + " WHERE " + SELECTION_TRACK_POSITIONS_UNSENT, bindArgs);

                    // update track
                    bindArgs = new String[]{trackUid, Integer.toString(TracksContract.STATUS_SENT), Long.toString(trackId), Integer.toString(TracksContract.STATUS_SENT)};
                    db.execSQL(UPDATE_TRACK_SENT + " WHERE " + SELECTION_TRACK_UNSENT, bindArgs);

                    // notify content resolver
                    getContentResolver().notifyChange(TracksContract.CONTENT_URI.buildUpon().appendPath(Long.toString(trackId)).build(), null);
                    getContentResolver().notifyChange(TracksContract.Position.CONTENT_URI, null);

                    result = true;
                }
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            if (trackCursor != null) trackCursor.close();
            if (positionCursor != null) positionCursor.close();
        }

        return result;
    }

    private boolean sendPositions(long trackId) throws IOException{
        boolean result = false;

        SQLiteDatabase db = mDatasource.getWritableDatabase();
        Cursor trackCursor = null;
        Cursor positionCursor = null;

        try {
            db.beginTransaction();

            // query track
            String[] selectionArgs = {Long.toString(trackId), Integer.toString(TracksContract.STATUS_SENT)};
            trackCursor = db.query(TrackDatasource.TABLE_TRACK, TracksContract.PROJECTION, SELECTION_TRACK_SENT, selectionArgs, null, null, null);
            TrackDatasource.TrackCursorHolder trackCursorHolder = new TrackDatasource.TrackCursorHolder(trackCursor);

            if (trackCursor.moveToFirst()) {
                // track has already been sent
                // query positions
                selectionArgs = new String[]{Long.toString(trackId), Integer.toString(TracksContract.Position.STATUS_SENT)};
                positionCursor = db.query(TrackDatasource.TABLE_POSITION, TracksContract.Position.PROJECTION, SELECTION_TRACK_POSITIONS_UNSENT, selectionArgs, null, null, null);
                TrackDatasource.PositionCursorHolder positionCursorHolder = new TrackDatasource.PositionCursorHolder(positionCursor);

                if (positionCursor.moveToFirst()) {
                    Log.d(TAG, "Sending positions for track {_id: " + trackId + "}");

                    // send positions
                    String trackUid = trackCursor.getString(trackCursorHolder.uidIndex);
                    String geoJson = loadGeoJsonGeometry(GEOJSON_POSITION_TEMPLATE, positionCursorHolder);
                    geoJson = loadGeoJsonProperties(geoJson, positionCursorHolder);
                    send("/api/v1/track/" + trackUid + "/position", "POST", cleanJson(geoJson));

                    // perform sql updates last so the RESERVED lock is held the least possible time
                    // update positions
                    String[] bindArgs = {Integer.toString(TracksContract.STATUS_SENT), Long.toString(trackId), Integer.toString(TracksContract.Position.STATUS_SENT)};
                    db.execSQL(UPDATE_POSITION_SENT + " WHERE " + SELECTION_TRACK_POSITIONS_UNSENT, bindArgs);

                    // notify content resolver
                    getContentResolver().notifyChange(TracksContract.Position.CONTENT_URI, null);

                    result = true;
                }
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            if (trackCursor != null) trackCursor.close();
            if (positionCursor != null) positionCursor.close();
        }

        return result;
    }

    private String loadGeoJsonProperties(String geoJson, long profileId) throws IOException {
        SQLiteDatabase db = mProfileDatasource.getReadableDatabase();
        Cursor profileCursor = null;
        Cursor contactCursor = null;

        try {
            // query profile
            String[] selectionArgs = {Long.toString(profileId)};
            profileCursor = db.query(ProfileDatasource.TABLE_PROFILE, ProfilesContract.PROJECTION, SELECTION_PROFILE, selectionArgs, null, null, null);
            ProfileDatasource.ProfileCursorHolder profileCursorHolder = new ProfileDatasource.ProfileCursorHolder(profileCursor);

            if (profileCursor.moveToFirst()) {
                geoJson = geoJson.replace("{{properties.name}}", profileCursor.getString(profileCursorHolder.nameIndex));
                geoJson = geoJson.replace("{{properties.activity}}", profileCursor.getString(profileCursorHolder.activityIndex));
                geoJson = geoJson.replace("{{properties.frequency}}", profileCursor.getString(profileCursorHolder.frequencyIndex));

                if (!profileCursor.isNull(profileCursorHolder.userNameIndex)) {
                    geoJson = geoJson.replace("{{properties.userName}}", profileCursor.getString(profileCursorHolder.userNameIndex));
                }

                int sharing = profileCursor.getInt(profileCursorHolder.sharingIndex);
                if (sharing > 0) {
                    String sharingJson = JSON_SHARING_TEMPLATE.replace("{{message}}", profileCursor.getString(profileCursorHolder.sharingMessageIndex));

                    if ((sharing & ProfilesContract.SHARING_EMAIL) > 0) {
                        // query contacts
                        selectionArgs = new String[]{Long.toString(profileId), ProfilesContract.Contact.TYPE_EMAIL};
                        contactCursor = db.query(ProfileDatasource.TABLE_CONTACT, ProfilesContract.Contact.PROJECTION, SELECTION_CONTACT, selectionArgs, null, null, null);
                        ProfileDatasource.ContactCursorHolder contactCursorHolder = new ProfileDatasource.ContactCursorHolder(contactCursor);

                        String sharingEmailJson = JSON_SHARING_EMAIL_TEMPLATE.replace("{{recipients}}", toJsonArray(contactCursor, contactCursorHolder.valueIndex));
                        sharingJson = sharingJson.replace("{{email}}", sharingEmailJson);
                    }

                    geoJson = geoJson.replace("{{properties.sharing}}", sharingJson);
                }
            }
        } finally {
            if (profileCursor != null) profileCursor.close();
            if (contactCursor != null) contactCursor.close();
        }

        return geoJson;
    }

    private String loadGeoJsonProperties(String geoJson, TrackDatasource.PositionCursorHolder cursorHolder) {
        String flag = getString(cursorHolder.getCursor(), cursorHolder.flagIndex, TracksContract.Position.FLAG_STOP);
        if (flag != null) {
            geoJson = geoJson.replace("{{properties.flag}}", flag);
        }
        return geoJson;
    }

    private String loadGeoJsonGeometry(String geoJson, TrackDatasource.PositionCursorHolder cursorHolder) {
        geoJson = geoJson.replace("{{geometry.coordinates}}", toGeoJsonCoordinates(cursorHolder));
        return geoJson;
    }

    private HttpURLConnection send(String path, String method, String payload) throws IOException {
        if (!isNetworkConnected(mConnectivityManager)) throw new DisconnectedNetworkException();

        Log.d(TAG, "Sending payload to path " + path);
        Log.d(TAG, payload);

        URL url = new URL(ENDPOINT + path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setRequestMethod(method);
        connection.setRequestProperty ("Content-Type", "application/json");
        connection.setRequestProperty ("Authorization", AUTHORIZATION_HEADER);

        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(payload);
        out.close();

        // server returns error if the track doesn't exist or is terminated
        int status = connection.getResponseCode();
        if (status == 200 || status == 201 || status == 404) return connection;
        throw new IOException("Server returned error " + status);
    }

    private void enableConnectivityReceiver() {
        Log.d(TAG, "Wait for network connectivity");
        ComponentName receiver = new ComponentName(this, ConnectivityReceiver.class);
        getPackageManager().setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }

    private void scheduleRecovery(long trackId, int count) {
        if (count >= MAX_RECOVER_COUNT) {
            Log.i(TAG, "Abort recovery after " + count + " attempts");
            return;
        }

        count++;
        Log.d(TAG, "Schedule recovery " + count);

        Intent intent = new Intent(this, GatewayService.class);
        intent.putExtra(EXTRA_TRACK_ID, trackId);
        intent.putExtra(EXTRA_RECOVERY_COUNT, count);
        PendingIntent pendingIntent = PendingIntent.getService(this, (int) trackId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (RECOVER_DELAY * count), pendingIntent);
    }

    private static String toGeoJsonCoordinates(TrackDatasource.PositionCursorHolder cursorHolder) {
        Cursor cursor = cursorHolder.getCursor();

        StringBuilder coordinatesBuilder = new StringBuilder();
        coordinatesBuilder.append("[");
        if (cursor.moveToFirst()) {
            do {
                if (!cursor.isFirst()) coordinatesBuilder.append(", ");
                coordinatesBuilder.append("[");
                coordinatesBuilder.append(cursor.getDouble(cursorHolder.latIndex) + ",");
                coordinatesBuilder.append(cursor.getDouble(cursorHolder.lngIndex) + ",");
                coordinatesBuilder.append(cursor.getDouble(cursorHolder.altitudeIndex) + ",");
                coordinatesBuilder.append(cursor.getLong(cursorHolder.timestampIndex));
                coordinatesBuilder.append("]");
            } while (cursor.moveToNext());
        }
        coordinatesBuilder.append("]");

        return coordinatesBuilder.toString();
    }

    private static String toJsonArray(Cursor cursor, int index) {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("[");
        cursor.moveToFirst();
        do {
            if (!cursor.isFirst()) jsonBuilder.append(", ");
            jsonBuilder.append("\"" + cursor.getString(index) + "\"");
        } while (cursor.moveToNext());
        jsonBuilder.append("]");

        return jsonBuilder.toString();
    }

    private static String cleanJson(String json) {
        json = json.replaceAll("\"\\w+\"\\s*:\\s*\"?\\{\\{[\\w\\.]+\\}\\}\"?,?", ""); // remove fields with variable value
        json = json.replaceAll(",+\\s*\\}", "}"); // remove extra comma
        return json;
    }

    private static String getString(Cursor cursor, int index, String value) {
        if (cursor.moveToFirst()) {
            do {
                if (value.equals(cursor.getString(index))) return value;
            } while (cursor.moveToNext());
        }
        return null;
    }

    private static boolean isNetworkConnected(ConnectivityManager connectivityManager) {
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    public static class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent serviceIntent = new Intent(context, GatewayService.class);
            serviceIntent.putExtra(EXTRA_TRACK_ID, intent.getLongExtra(EXTRA_TRACK_ID, -1));
            context.startService(serviceIntent);
        }
    }

    public static class ConnectivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (isNetworkConnected(connectivityManager)) {
                Log.i(TAG, "Network is back. Attempt to send tracks");

                // disable connectivity broadcast receiver
                ComponentName receiver = new ComponentName(context, ConnectivityReceiver.class);
                context.getPackageManager().setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

                // start service
                Intent serviceIntent = new Intent(context, GatewayService.class);
                context.startService(serviceIntent);
            }
        }
    }
}
