package com.livepeak.mobile.track;

import android.net.Uri;

/**
 * Created by guillaume on 16/05/15.
 */
public class TracksContract {

    public static final String AUTHORITY = "com.livepeak.mobile.track.provider";

    public static final String BASE_PATH = "tracks";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    public static final String UID = "uid";

    public static final String DURATION = "duration";

    public static final String DISTANCE = "distance";

    public static final String STATE = "state";

    public static final String STATUS = "status";

    public static final String PROFILE_ID = "profile_id";

    public static final String[] PROJECTION = {"_id", UID, DURATION, DISTANCE, STATE, STATUS, PROFILE_ID};

    public static final String STATE_IDLE = "Idle";

    public static final String STATE_RUNNING = "Running";

    public static final String STATE_LOST = "Lost";

    public static final String STATE_TERMINATED = "Terminated";

    public static final int STATUS_SENT = 1;

    public static class Position {

        public static final String BASE_PATH = TracksContract.BASE_PATH + "/positions";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(TracksContract.CONTENT_URI, "positions");

        public static final String LAT = "lat";

        public static final String LNG = "lng";

        public static final String ALTITUDE = "altitude";

        public static final String BEARING = "bearing";

        public static final String TIMESTAMP = "timestamp";

        public static final String STATUS = "status";

        public static final String FLAG = "flag";

        public static final String TRACK_ID = "track_id";

        public static final String[] PROJECTION = {"_id", LAT, LNG, ALTITUDE, BEARING, TIMESTAMP, STATUS, FLAG, TRACK_ID};

        public static final int STATUS_SENT = 1;

        public static final String FLAG_STOP = "Stop";
    }
}
