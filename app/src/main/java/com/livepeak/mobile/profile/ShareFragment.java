package com.livepeak.mobile.profile;

import android.app.Fragment;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.livepeak.mobile.R;
import com.livepeak.mobile.profile.contact.ContactListActivity;

/**
 * Created by guillaume on 12/01/15.
 */
public class ShareFragment extends Fragment {

    private static final int TOKEN_SHARE_EMAIL = 10;

    private static final int TOKEN_CONTACT_LIST_RESULT = 20;

    private static final int REQUEST_CONTACT_LIST = 10;

    private static final String SELECTION_CONTACT = ProfilesContract.Contact.PROFILE_ID + " = ?";

    private static final String[] PROJECTION_CONTACT_COUNT = {"count(*)"};

    private Profile mProfile;

    private EditText mMessageText;

    private ShareView mEmailShare;

    private ContactQueryHandler mContactQueryHandler;

    private boolean mTextChangeEnabled = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContactQueryHandler = new ContactQueryHandler(getActivity().getContentResolver());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_share_fragment, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        OnListener listener = new OnListener();

        mMessageText = (EditText) getView().findViewById(R.id.message_text);
        mMessageText.addTextChangedListener(new TextChangeListener(mMessageText));
        mEmailShare = (ShareView) getView().findViewById(R.id.email_share);
        mEmailShare.setOnClickListener(listener);
        mEmailShare.setOnCheckedChangeListener(listener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONTACT_LIST:
                mContactQueryHandler.startQuery(TOKEN_CONTACT_LIST_RESULT, mProfile.getId());
                break;
        }
    }

    public void setProfile(Profile profile) {
        mProfile = profile;

        mTextChangeEnabled = false;
        mMessageText.setText(profile.getSharingMessage());
        mEmailShare.setSharing((profile.getSharing() & ProfilesContract.SHARING_EMAIL) > 0);
        mTextChangeEnabled = true;
    }

    private class OnListener implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.email_share:
                    Intent intent = new Intent(getActivity(), ContactListActivity.class);
                    intent.putExtra(ContactListActivity.EXTRA_PROFILE_ID, mProfile.getId());
                    startActivityForResult(intent, REQUEST_CONTACT_LIST);
                    break;
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton view, boolean isChecked) {
            switch (view.getId()) {
                case ShareView.R_ID_ENABLE_SWITCH:
                    if (isChecked) {
                        mProfile.setSharing(mProfile.getSharing() | ProfilesContract.SHARING_EMAIL);
                        mContactQueryHandler.startQuery(TOKEN_SHARE_EMAIL, mProfile.getId());
                    } else {
                        mProfile.setSharing(mProfile.getSharing() & ~ProfilesContract.SHARING_EMAIL);
                    }
                    setProfile(mProfile);
                    break;
            }
        }
    }

    private class TextChangeListener implements TextWatcher {

        private TextView mView;

        public TextChangeListener(TextView view) {
            mView = view;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!mTextChangeEnabled) return;

            String text = s.length() > 0 ? s.toString() : null;
            switch(mView.getId()) {
                case R.id.message_text:
                    mProfile.setSharingMessage(text);
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

    private class ContactQueryHandler extends AsyncQueryHandler {

        public ContactQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void startQuery(int token, long profileId) {
            String[] selectionArgs = {Long.toString(profileId)};
            startQuery(token, null, ProfilesContract.Contact.CONTENT_URI, PROJECTION_CONTACT_COUNT, SELECTION_CONTACT, selectionArgs, null);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            switch(token) {
                case TOKEN_SHARE_EMAIL:
                    if (cursor.moveToFirst()) {
                        int contactCount = cursor.getInt(0);
                        if (contactCount == 0) {
                            Intent intent = new Intent(getActivity(), ContactListActivity.class);
                            intent.putExtra(ContactListActivity.EXTRA_PROFILE_ID, mProfile.getId());
                            startActivityForResult(intent, REQUEST_CONTACT_LIST);
                        }
                    }
                    break;
                case TOKEN_CONTACT_LIST_RESULT:
                    if (cursor.moveToFirst()) {
                        int contactCount = cursor.getInt(0);
                        if (contactCount == 0) {
                            mProfile.setSharing(mProfile.getSharing() & ~ProfilesContract.SHARING_EMAIL);
                            setProfile(mProfile);
                        }
                    }
                    break;
            }
        }
    }

}
