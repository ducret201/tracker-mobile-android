package com.livepeak.mobile.profile.contact;

import android.content.ContentValues;

import com.livepeak.mobile.profile.ProfilesContract;

/**
 * Created by guillaume on 14/05/15.
 */
public class Contact {

    private final Long mId;

    private final String mName;

    private final String mType;

    private final String mValue;

    private final long mProfileId;

    public Contact(long profileId, String name, String email) {
        mId = null;
        mType = ProfilesContract.Contact.TYPE_EMAIL;
        mProfileId = profileId;
        mName = name;
        mValue = email;
    }

    public Long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getValue() {
        return mValue;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ProfilesContract.Contact.NAME, mName);
        values.put(ProfilesContract.Contact.TYPE, mType);
        values.put(ProfilesContract.Contact.VALUE, mValue);
        values.put(ProfilesContract.Contact.PROFILE_ID, mProfileId);
        return values;
    }
}
