package com.livepeak.mobile.profile;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.livepeak.mobile.R;
import com.livepeak.mobile.profile.activity.ActivityImageView;
import com.livepeak.mobile.profile.activity.ActivityListActivity;

/**
 * Created by guillaume on 14/05/15.
 */
public class ProfileFragment extends Fragment {

    private static final int REQUEST_ACTIVITY_PICKER = 10;

    private ActivityImageView mActivityImage;

    private EditText mNameText;

    private TextView mByText;

    private EditText mUserNameText;

    private Profile mProfile;

    private boolean mTextChangeEnabled = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mActivityImage = (ActivityImageView) getView().findViewById(R.id.activity_image);
        mActivityImage.setOnClickListener(new OnClickListener());
        mNameText = (EditText) getView().findViewById(R.id.name_text);
        mNameText.addTextChangedListener(new TextChangeListener(mNameText));
        mByText = (TextView) getView().findViewById(R.id.by_text);
        mUserNameText = (EditText) getView().findViewById(R.id.user_name_text);
        mUserNameText.addTextChangedListener(new TextChangeListener(mUserNameText));
    }

    public void setProfile(Profile profile) {
        mProfile = profile;

        mTextChangeEnabled = false;
        mActivityImage.setActivity(profile.getActivity());
        mNameText.setText(profile.getName());
        mUserNameText.setText(profile.getUserName());
        setByTextVisibility();
        mTextChangeEnabled = true;
    }

    private void setByTextVisibility() {
        if (mProfile.getUserName() == null) {
            mByText.setVisibility(View.GONE);
        } else {
            mByText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_ACTIVITY_PICKER:
                    mProfile.setActivity(data.getStringExtra(ActivityListActivity.EXTRA_ACTIVITY));
                    mProfile.setName(getResources().getString(Profile.getDefaultNameResourceId(getActivity(), mProfile.getActivity())));
                    mProfile.setFrequency(Profile.getDefaultFrequency(mProfile.getActivity()));
                    ((ProfileActivity) getActivity()).setProfile(mProfile); // update all fragments
                    break;
            }
        }
    }

    private class OnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ProfileFragment.this.getActivity(), ActivityListActivity.class);
            intent.putExtra(ActivityListActivity.EXTRA_ACTIVITY, mProfile.getActivity());
            startActivityForResult(intent, REQUEST_ACTIVITY_PICKER);
        }
    }

    private class TextChangeListener implements TextWatcher {

        private TextView mView;

        public TextChangeListener(TextView view) {
            mView = view;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!mTextChangeEnabled) return;

            String text = s.length() > 0 ? s.toString() : null;
            switch(mView.getId()) {
                case R.id.name_text:
                    mProfile.setName(text);
                    break;
                case R.id.user_name_text:
                    mProfile.setUserName(text);
                    setByTextVisibility();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

}
