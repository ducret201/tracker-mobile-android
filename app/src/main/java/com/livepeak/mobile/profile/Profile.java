package com.livepeak.mobile.profile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by guillaume on 27/04/15.
 */
public class Profile implements Parcelable {

    private Long mId;

    private String mName;

    private String mUserName;

    private String mActivity;

    private Integer mSharing;

    private String mSharingMessage;

    private Integer mFrequency;

    private String mState;

    public Profile() {
        mActivity = ProfilesContract.ACTIVITY_WALKING;
        mSharing = 0;
        mFrequency = 10;
        mState = ProfilesContract.STATE_IDLE;
    }

    public Profile(Parcel in) {
        mId = in.readLong();
        mName = in.readString();
        mUserName = in.readString();
        mActivity = in.readString();
        mSharing = in.readInt();
        mSharingMessage = in.readString();
        mFrequency = in.readInt();
        mState = in.readString();
    }

    public Profile(Cursor cursor) {
        ProfileDatasource.ProfileCursorHolder cursorHolder = new ProfileDatasource.ProfileCursorHolder(cursor);
        mId = cursor.getLong(cursorHolder.idIndex);
        mName = cursor.getString(cursorHolder.nameIndex);
        mUserName = cursor.getString(cursorHolder.userNameIndex);
        mActivity = cursor.getString(cursorHolder.activityIndex);
        mSharing = cursor.getInt(cursorHolder.sharingIndex);
        mSharingMessage = cursor.getString(cursorHolder.sharingMessageIndex);
        mFrequency = cursor.getInt(cursorHolder.frequencyIndex);
        mState = cursor.getString(cursorHolder.stateIndex);
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getActivity() {
        return mActivity;
    }

    public void setActivity(String activity) {
        mActivity = activity;
    }

    public int getSharing() {
        return mSharing;
    }

    public void setSharing(int sharing) {
        mSharing = sharing;
    }

    public String getSharingMessage() {
        return mSharingMessage;
    }

    public void setSharingMessage(String sharingMessage) {
        mSharingMessage = sharingMessage;
    }

    public int getFrequency() {
        return mFrequency;
    }

    public void setFrequency(int frequency) {
        mFrequency = frequency;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mName);
        dest.writeString(mUserName);
        dest.writeString(mActivity);
        dest.writeInt(mSharing);
        dest.writeString(mSharingMessage);
        dest.writeInt(mFrequency);
        dest.writeString(mState);
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ProfilesContract.NAME, mName);
        values.put(ProfilesContract.USER_NAME, mUserName);
        values.put(ProfilesContract.ACTIVITY, mActivity);
        values.put(ProfilesContract.SHARING, mSharing);
        values.put(ProfilesContract.SHARING_MESSAGE, mSharingMessage);
        values.put(ProfilesContract.FREQUENCY, mFrequency);
        values.put(ProfilesContract.STATE, mState);
        return values;
    }

    public static int getDefaultNameResourceId(Context context, String activityName) {
        String resourceName = "profile_name_" + activityName.replaceAll("(.)([A-Z])", "$1_$2").toLowerCase();
        return context.getResources().getIdentifier(resourceName, "string", context.getPackageName());
    }

    public static int getDefaultFrequency(String activityName) {
        int frequency = 60;

        if (ProfilesContract.ACTIVITY_WALKING.equals(activityName)) {
            frequency = 600; // 10 min
        } else if (ProfilesContract.ACTIVITY_RUNNING.equals(activityName)) {
            frequency = 300; // 5 min
        } else if (ProfilesContract.ACTIVITY_TREKKING.equals(activityName)) {
            frequency = 600; // 10 min
        } else if (ProfilesContract.ACTIVITY_BIKING.equals(activityName)) {
            frequency = 60; // 1 min
        } else if (ProfilesContract.ACTIVITY_MOUNTAIN_BIKING.equals(activityName)) {
            frequency = 60; // 1 min
        } else if (ProfilesContract.ACTIVITY_SKIING.equals(activityName)) {
            frequency = 60; // 1 min
        } else if (ProfilesContract.ACTIVITY_PARAGLIDING.equals(activityName)) {
            frequency = 60; // 1 min
        } else if (ProfilesContract.ACTIVITY_HANGGLIDING_.equals(activityName)) {
            frequency = 30; // 30 s
        } else if (ProfilesContract.ACTIVITY_CAR.equals(activityName)) {
            frequency = 30; // 30 s
        } else if (ProfilesContract.ACTIVITY_MOTORCYCLE.equals(activityName)) {
            frequency = 30; // 30 s
        } else if (ProfilesContract.ACTIVITY_PUBLIC_TRANSPORT.equals(activityName)) {
            frequency = 60; // 1 min
        }

        return frequency;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
