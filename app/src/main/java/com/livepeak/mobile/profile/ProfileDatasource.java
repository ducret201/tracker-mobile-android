package com.livepeak.mobile.profile;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.livepeak.android.database.CursorHolder;


/**
 * Created by guillaume on 28/04/15.
 */
public class ProfileDatasource extends SQLiteOpenHelper {

    private static final String DB_NAME = "profile.db";

    private static final int DB_VERSION = 1;

    public static final String TABLE_PROFILE = "profile";

    public static final String TABLE_CONTACT = "contact";

    public static final String COLUMN_ID = "_id";

    private static final String CREATE_TABLE_PROFILE = "CREATE TABLE " + TABLE_PROFILE + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            ProfilesContract.NAME + " TEXT NOT NULL, " +
            ProfilesContract.USER_NAME + " TEXT, " +
            ProfilesContract.ACTIVITY + " TEXT NOT NULL, " +
            ProfilesContract.SHARING + " INTEGER DEFAULT 0 NOT NULL," +
            ProfilesContract.SHARING_MESSAGE + " TEXT, " +
            ProfilesContract.FREQUENCY + " INTEGER NOT NULL," +
            ProfilesContract.STATE + " TEXT NOT NULL" +
            ")";

    public static final String CREATE_TABLE_CONTACT = "CREATE TABLE " + TABLE_CONTACT + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            ProfilesContract.Contact.NAME + " TEXT NOT NULL, " +
            ProfilesContract.Contact.TYPE + " TEXT NOT NULL, " +
            ProfilesContract.Contact.VALUE + " TEXT NOT NULL," +
            ProfilesContract.Contact.PROFILE_ID + " INTEGER REFERENCES " + TABLE_PROFILE + "(" + COLUMN_ID + ") NOT NULL" +
            ")";

    public ProfileDatasource(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PROFILE);
        db.execSQL(CREATE_TABLE_CONTACT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static class ProfileCursorHolder extends CursorHolder {

        public final int nameIndex;

        public final int userNameIndex;

        public final int activityIndex;

        public final int sharingMessageIndex;

        public final int sharingIndex;

        public final int frequencyIndex;

        public final int stateIndex;

        public ProfileCursorHolder(Cursor cursor) {
            super(cursor);

            this.nameIndex = cursor.getColumnIndex(ProfilesContract.NAME);
            this.userNameIndex = cursor.getColumnIndex(ProfilesContract.USER_NAME);
            this.activityIndex = cursor.getColumnIndex(ProfilesContract.ACTIVITY);
            this.sharingMessageIndex = cursor.getColumnIndex(ProfilesContract.SHARING_MESSAGE);
            this.sharingIndex = cursor.getColumnIndex(ProfilesContract.SHARING);
            this.frequencyIndex = cursor.getColumnIndex(ProfilesContract.FREQUENCY);
            this.stateIndex = cursor.getColumnIndex(ProfilesContract.STATE);
        }
    }

    public static class ContactCursorHolder extends CursorHolder {

        public final int nameIndex;

        public final int valueIndex;

        public ContactCursorHolder(Cursor cursor) {
            super(cursor);

            this.nameIndex = cursor.getColumnIndex(ProfilesContract.Contact.NAME);
            this.valueIndex = cursor.getColumnIndex(ProfilesContract.Contact.VALUE);
        }
    }
}
