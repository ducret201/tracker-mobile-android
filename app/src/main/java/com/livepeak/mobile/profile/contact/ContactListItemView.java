package com.livepeak.mobile.profile.contact;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.TextView;

import com.livepeak.android.view.DismissView;
import com.livepeak.mobile.R;
import com.livepeak.mobile.profile.ProfileDatasource;

/**
 * Created by guillaume on 27/04/15.
 */
public class ContactListItemView extends DismissView {

    private TextView mNameText;

    private TextView mValueText;

    public ContactListItemView(Context context) {
        super(context);
        init();
    }

    public ContactListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ContactListItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.profile_contact_list_item_view, this);

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);

        mNameText = (TextView) findViewById(R.id.name_text);
        mValueText = (TextView) findViewById(R.id.value_text);
    }

    public void setContact(ProfileDatasource.ContactCursorHolder cursorHolder) {
        reset(false);
        Cursor cursor = cursorHolder.getCursor();
        mNameText.setText(cursor.getString(cursorHolder.nameIndex));
        mValueText.setText(cursor.getString(cursorHolder.valueIndex));
    }
}
