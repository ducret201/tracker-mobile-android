package com.livepeak.mobile.profile;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.livepeak.android.view.DismissView;
import com.livepeak.mobile.R;
import com.livepeak.mobile.profile.activity.ActivityImageView;

/**
 * Created by guillaume on 27/04/15.
 */
public class ProfileListItemView extends DismissView {

    private TextView mUndoText;

    private ActivityImageView mActivityImage;

    private TextView mNameText;

    public ProfileListItemView(Context context) {
        super(context);
        init();
    }

    public ProfileListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProfileListItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.profile_list_item_view, this);

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);

        mUndoText = (TextView) findViewById(R.id.undo_text);
        mUndoText.setOnClickListener(new OnClickListener());
        mActivityImage = (ActivityImageView) findViewById(R.id.activity_image);
        mNameText = (TextView) findViewById(R.id.name_text);
    }

    public void setProfile(ProfileDatasource.ProfileCursorHolder cursorHolder) {
        reset(false);

        Cursor cursor = cursorHolder.getCursor();
        mActivityImage.setActivity(cursor.getString(cursorHolder.activityIndex));
        mNameText.setText(cursor.getString(cursorHolder.nameIndex));
    }

    private class OnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.undo_text:
                    mlistener.onUndo(ProfileListItemView.this);
                    break;
            }
        }
    }
}
