package com.livepeak.mobile.profile;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.livepeak.mobile.R;

/**
 * Created by guillaume on 08/01/15.
 */
public class ShareView extends RelativeLayout {

    public static final int R_ID_ENABLE_SWITCH = R.id.enable_switch;

    public static final int TYPE_EMAIL = 0;

    private ImageView mTypeImage;

    private TextView mTypeText;

    private Switch mEnableSwitch;

    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener;

    private int mType;

    public ShareView(Context context, AttributeSet attrs) {
        super(context, attrs, R.attr.rowStyle);
        init(context, attrs);
    }

    public ShareView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(getContext(), R.layout.profile_share_view, this);

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);

        TypedArray styledAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ActivityImageView, 0, 0);
        try {
            mType = styledAttrs.getInteger(R.styleable.ShareView_share_type, 0);
        } finally {
            styledAttrs.recycle();
        }

        mTypeImage = (ImageView) findViewById(R.id.type_image);
        mTypeText = (TextView) findViewById(R.id.type_text);
        mEnableSwitch = (Switch) findViewById(R.id.enable_switch);

        switch(mType) {
            case TYPE_EMAIL:
                mTypeImage.setImageResource(R.drawable.ic_action_email_light);
                mTypeText.setText(getResources().getString(R.string.profile_share_email));
                break;
        }
    }

    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener listener) {
        mOnCheckedChangeListener = listener;
        mEnableSwitch.setOnCheckedChangeListener(listener);
    }

    public void setSharing(boolean sharing) {
        mEnableSwitch.setOnCheckedChangeListener(null);
        mEnableSwitch.setChecked(sharing);
        mEnableSwitch.setOnCheckedChangeListener(mOnCheckedChangeListener);
    }
}
