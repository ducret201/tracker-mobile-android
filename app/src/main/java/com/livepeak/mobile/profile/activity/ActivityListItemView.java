package com.livepeak.mobile.profile.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.livepeak.mobile.R;

/**
 * Created by guillaume on 20/05/15.
 */
public class ActivityListItemView extends RelativeLayout {

    private ActivityImageView mActivityImage;

    private TextView mActivityText;

    public ActivityListItemView(Context context) {
        super(context, null, R.attr.rowStyle);
        init();
    }

    public ActivityListItemView(Context context, AttributeSet attrs) {
        super(context, attrs, R.attr.rowStyle);
        init();
    }

    public ActivityListItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.profile_activity_list_item_view, this);

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);

        mActivityImage = (ActivityImageView) findViewById(R.id.activity_image);
        mActivityText = (TextView) findViewById(R.id.activity_text);
    }

    public void setActivity(String activity) {
        mActivityImage.setActivity(activity);
        mActivityText.setText(activity.replaceAll("(.)([A-Z])", "$1 $2"));
    }
}
