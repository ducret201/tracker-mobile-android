package com.livepeak.mobile.profile;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.livepeak.mobile.R;

/**
 * Created by guillaume on 09/01/15.
 */
public class PowerFragment extends Fragment {

    private Profile mProfile;

    private TextView mFrequencyText;

    private SeekBar mFrquencySeekBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_power_fragment, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mFrequencyText = (TextView) getView().findViewById(R.id.frequency_text);
        mFrquencySeekBar = (SeekBar) getView().findViewById(R.id.frequency_seekbar);
        mFrquencySeekBar.setMax(100);
        mFrquencySeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener());
    }

    public void setProfile(Profile profile) {
        mProfile = profile;
        mFrequencyText.setText(getResources().getString(R.string.profile_power_frequency) + " " + formatValue(mProfile.getFrequency()));
        mFrquencySeekBar.setProgress(interpolateProgress(mProfile.getFrequency()));
    }

    private void setProgress(int progress) {
        int value = interpolateValue(progress);
        if (value != mProfile.getFrequency()) {
            mProfile.setFrequency(value);
            mFrequencyText.setText(getResources().getString(R.string.profile_power_frequency) + " " + formatValue(mProfile.getFrequency()));
        }
    }

    private int interpolateValue(int progress) {
        if (progress <= 25) {
            int value = mapRange(0, 25, 10, 60, progress); // range [0,60s]
            return value / 10 * 10; // round 10s
        }

        if (progress <= 50) {
            int value = mapRange(25, 50, 60, 60 * 10, progress); // range ]1,10min]
            return value / 60 * 60; // round 1min
        }

        if (progress <= 75) {
            int value = mapRange(50, 75, 60 * 10, 3600, progress); // range ]10,60min]
            return value / 600 * 600; // round 10min
        }

        if (progress <= 100) {
            int value = mapRange(75, 100, 3600, 3600 * 6, progress); // range ]1,6h]
            return value / 3600 * 3600; // round 1h
        }

        throw new RuntimeException("Position is not in range [0,100]");
    }

    private int interpolateProgress(int value) {
        if (value <= 60) {
            return mapRange(10, 60, 0, 25, value); // range [0,25]
        }

        if (value <= 60 * 10) {
            return mapRange(60, 60 * 10, 25, 50, value); // range ]25,50]
        }

        if (value <= 3600) {
            return mapRange(60 * 10, 3600, 50, 75, value); // range ]50,75]
        }

        if (value <= 3600 * 6) {
            return mapRange(3600, 3600 * 6, 75, 100, value); // range ]75,100]
        }

        throw new RuntimeException("Value is not in range [10s,6h]");
    }

    private String formatValue(int value) {
        if (value < 60) {
            return value + " s";
        }

        if (value < 3600) {
            return (value / 60) + " min";
        }

        return (value / 3600) + "h";
    }

    private int mapRange(int a1, int a2, int b1, int b2, int v) {
        return b1 + (v - a1)*(b2 - b1)/(a2 - a1);
    }

    private class OnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            setProgress(progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }
}
