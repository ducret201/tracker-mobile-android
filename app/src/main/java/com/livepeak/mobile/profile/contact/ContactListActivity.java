package com.livepeak.mobile.profile.contact;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.livepeak.android.view.CursorRecyclerViewAdapter;
import com.livepeak.android.view.DismissView;
import com.livepeak.mobile.R;
import com.livepeak.mobile.profile.ProfileDatasource;
import com.livepeak.mobile.profile.ProfileListActivity;
import com.livepeak.mobile.profile.ProfilesContract;

public class ContactListActivity extends ActionBarActivity {

    private static final int TOKEN_DELETE = 10;

    public static final String EXTRA_PROFILE_ID = "PROFILE_ID";

    private final static String SELECTION_CONTACT = ProfilesContract.Contact.PROFILE_ID + " = ?";

    private static final int REQUEST_CONTACT_PICKER = 10;

    private ContactQueryHandler mContactQueryHandler;

    private RecyclerView mContactsRecycler;

    private ViewGroup mContactsEmptyLayout;

    private ContactsAdapter mContactsAdapter;

    private long mProfileId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_contact_list_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ContactLoaderCallbacks loaderCallbacks = new ContactLoaderCallbacks();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        mContactQueryHandler = new ContactQueryHandler(getContentResolver());
        mContactsAdapter = new ContactsAdapter(this, null);
        mContactsRecycler = (RecyclerView) findViewById(R.id.contacts_recycler);
        mContactsRecycler.setHasFixedSize(true);
        mContactsRecycler.setLayoutManager(layoutManager);
        mContactsRecycler.setAdapter(mContactsAdapter);
        mContactsEmptyLayout = (ViewGroup) findViewById(R.id.contacts_empty_layout);
        mContactsEmptyLayout.setOnClickListener(new OnClickListener());

        Intent intent = getIntent();
        mProfileId = intent.getLongExtra(EXTRA_PROFILE_ID, -1);

        getLoaderManager().initLoader(0, null, loaderCallbacks);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mContactsAdapter.setNotifyDataset(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_contact_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, REQUEST_CONTACT_PICKER);
                return true;
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    upIntent = new Intent(this, ProfileListActivity.class); // can't recreate direct parent without extra
                    TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
                } else {
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CONTACT_PICKER:
                    addContact(data.getData());
                    break;
            }
        }
    }

    private void setCursor(Cursor cursor) {
        mContactsAdapter.swapCursor(cursor);
        if (mContactsAdapter.isNotifyDataset()) {

            if (cursor == null || cursor.getCount() == 0) {
                mContactsRecycler.setVisibility(View.INVISIBLE);
                mContactsEmptyLayout.setVisibility(View.VISIBLE);
            } else {
                mContactsRecycler.setVisibility(View.VISIBLE);
                mContactsEmptyLayout.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void addContact(Uri uri) {
        Contact contact = queryContact(uri);
        if (contact != null) {
            mContactQueryHandler.startInsert(0, contact);
        } else {
            Toast.makeText(this, "Contact has no email", Toast.LENGTH_SHORT).show();
        }
    }

    private Contact queryContact(Uri uri) {
        String id = uri.getLastPathSegment();
        ContentResolver contentResolver = getContentResolver();

        String name = null;
        String email = null;

        // query name
        Cursor cursor = contentResolver.query(uri, null, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }
        } finally {
            if (cursor != null) cursor.close();
        }

        // query emails
        String selection = ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?";
        String[] selectionArgs = new String[]{id};
        cursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, selection, selectionArgs, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            }
        } finally {
            if (cursor != null) cursor.close();
        }

        if (name != null && email != null) return new Contact(mProfileId, name, email);
        return null;
    }

    private class ContactsAdapter extends CursorRecyclerViewAdapter<ContactsAdapter.ViewHolder, ProfileDatasource.ContactCursorHolder> {

        private static final int TYPE_CONTACT = 10;

        private int mDelayedRemovedPosition = -1;

        private ContactsAdapter(Context context, Cursor cursor) {
            super(context, cursor);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(new ContactListItemView(parent.getContext()));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, Cursor cursor, ProfileDatasource.ContactCursorHolder cursorHolder) {
            holder.setContact(cursorHolder);
        }

        @Override
        protected ProfileDatasource.ContactCursorHolder onCreateCursorHolder(Cursor cursor) {
            return new ProfileDatasource.ContactCursorHolder(cursor);
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_CONTACT;
        }

        public void remove(int position) {
            if (mDelayedRemovedPosition >= 0) {
                notifyItemRemoved(mDelayedRemovedPosition);

                if (mDelayedRemovedPosition < position) {
                    position--;
                }
            }
            mDelayedRemovedPosition = position;
        }

        public void flush() {
            if (mDelayedRemovedPosition >= 0) {
                notifyItemRemoved(mDelayedRemovedPosition);
                mDelayedRemovedPosition = -1;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private long mContactId;

            public ViewHolder(ContactListItemView view) {
                super(view);

                view.setOnDismissListener(new DismissView.OnDismissListener() {
                    @Override
                    public void onDismiss(DismissView view) {
                        mContactsAdapter.setNotifyDataset(false);
                        mContactQueryHandler.startDelete(TOKEN_DELETE, ViewHolder.this, mContactId);
                    }

                    @Override
                    public void onUndo(DismissView view) {
                    }
                });
            }

            public ContactListItemView getView() {
                return (ContactListItemView) itemView;
            }

            public void setContact(ProfileDatasource.ContactCursorHolder cursorHolder) {
                Cursor cursor = cursorHolder.getCursor();
                mContactId = cursor.getLong(cursorHolder.idIndex);

                getView().setContact(cursorHolder);
            }
        }
    }

    private class ContactLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            String[] projection = {ProfileDatasource.COLUMN_ID, ProfilesContract.Contact.NAME, ProfilesContract.Contact.VALUE};
            String[] selectionArgs = {Long.toString(mProfileId)};
            CursorLoader cursorLoader = new CursorLoader(ContactListActivity.this, ProfilesContract.Contact.CONTENT_URI, projection, SELECTION_CONTACT, selectionArgs, null);
            return cursorLoader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            setCursor(data);
            mContactsAdapter.flush();
            mContactsAdapter.setNotifyDataset(true);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            setCursor(null);
        }
    }

    private class ContactQueryHandler extends AsyncQueryHandler {

        public ContactQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void startInsert(int token, Contact contact) {
            if (contact.getId() != null) return;
            super.startInsert(token, contact, ProfilesContract.Contact.CONTENT_URI, contact.toContentValues());
        }

        public void startDelete(int token, ContactsAdapter.ViewHolder viewHolder, long contactId) {
            Uri uri = ProfilesContract.Contact.CONTENT_URI.buildUpon().appendPath(String.valueOf(contactId)).build();
            super.startDelete(token, viewHolder, uri, null, null);
        }

        @Override
        protected void onDeleteComplete(int token, Object cookie, int result) {
            ContactsAdapter.ViewHolder viewHolder = (ContactsAdapter.ViewHolder) cookie;

            switch (token) {
                case TOKEN_DELETE:
                    mContactsAdapter.remove(viewHolder.getAdapterPosition());
                    break;
            }
        }
    }

    private class OnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.contacts_empty_layout:
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, REQUEST_CONTACT_PICKER);
                    break;
            }
        }
    }
}
