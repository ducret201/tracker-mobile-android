package com.livepeak.mobile.profile.activity;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.livepeak.mobile.R;


/**
 * Created by guillaume on 12/05/15.
 */
public class ActivityImageView extends ImageView {

    private static final int SIZE_LARGE = 1;

    private int mSize;

    public ActivityImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ActivityImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray styledAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ActivityImageView, 0, 0);

        try {
            mSize = styledAttrs.getInteger(R.styleable.ActivityImageView_size, 0);
        } finally {
            styledAttrs.recycle();
        }

        setImageResource(getDefaultResourceId());
    }

    public void setActivity(String activity) {
        setImageResource(getActivityResourceId(activity));
    }

    private int getActivityResourceId(String activityName) {
        if (activityName == null) return getDefaultResourceId();

        String name = "ic_activity_" + activityName.replaceAll("(.)([A-Z])", "$1_$2").toLowerCase();
        if (mSize == SIZE_LARGE) {
            name = name + "_large";
        }

        Resources resources = getContext().getResources();
        return resources.getIdentifier(name, "drawable", getContext().getPackageName());
    }

    private int getDefaultResourceId() {
        if (mSize == SIZE_LARGE) return R.drawable.ic_activity_walking_large;
        return R.drawable.ic_activity_walking;
    }
}
