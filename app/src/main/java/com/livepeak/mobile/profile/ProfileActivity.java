package com.livepeak.mobile.profile;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.livepeak.mobile.R;
import com.livepeak.mobile.app.TrackerApplication;
import com.livepeak.mobile.track.TrackActivity;

public class ProfileActivity extends ActionBarActivity {

    public static final String EXTRA_PROFILE_ID = "ID";

    private static final int TOKEN_TRACK = 10;

    private ProfileFragment mProfileFragment;

    private ShareFragment mShareFragment;

    private PowerFragment mPowerFragment;

    private ProfileQueryHandler mProfileQueryHandler;

    private Profile mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProfileFragment = (ProfileFragment) getFragmentManager().findFragmentById(R.id.profile_fragment);
        mShareFragment = (ShareFragment) getFragmentManager().findFragmentById(R.id.share_fragment);
        mPowerFragment = (PowerFragment) getFragmentManager().findFragmentById(R.id.power_fragment);
        mProfileQueryHandler = new ProfileQueryHandler(getContentResolver());

        Intent intent = getIntent();
        long profileId = intent.getLongExtra(EXTRA_PROFILE_ID, -1);
        if (profileId >= 0) {
            mProfileQueryHandler.startQuery(profileId);
        } else {
            mProfileQueryHandler.startInsert(0, createProfile());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mProfileQueryHandler.startUpdate(0, validate(mProfile));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_track:
                mProfile.setState(ProfilesContract.STATE_ACTIVE);
                mProfileQueryHandler.startUpdate(TOKEN_TRACK, validate(mProfile));
                return true;
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
                } else {
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Profile createProfile() {
        SharedPreferences prefs = getSharedPreferences(TrackerApplication.APP_PREFS, MODE_PRIVATE);

        Profile profile = new Profile();
        profile.setName(getResources().getString(Profile.getDefaultNameResourceId(this, profile.getActivity())));
        profile.setUserName(prefs.getString(TrackerApplication.PREFS_USERNAME, null));
        profile.setSharingMessage(getResources().getString(R.string.profile_share_message));
        profile.setFrequency(Profile.getDefaultFrequency(profile.getActivity()));
        return profile;
    }

    private Profile validate(Profile profile) {
        if (profile == null) return null;

        if (profile.getName() == null) {
            profile.setName(getResources().getString(Profile.getDefaultNameResourceId(this, profile.getActivity())));
        }
        if (profile.getSharingMessage() == null) {
            profile.setSharingMessage(getResources().getString(R.string.profile_share_message));
        }
        return profile;
    }

    public void setProfile(Profile profile) {
        mProfile = profile;
        mProfileFragment.setProfile(profile);
        mShareFragment.setProfile(profile);
        mPowerFragment.setProfile(profile);
    }

    private class ProfileQueryHandler extends AsyncQueryHandler {

        public ProfileQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void startQuery(long id) {
            Uri uri = ProfilesContract.CONTENT_URI.buildUpon().appendPath(String.valueOf(id)).build();
            startQuery(0, null, uri, ProfilesContract.PROJECTION, null, null, null);
        }

        public void startInsert(int token, Profile profile) {
            if (profile == null || profile.getId() != null) return;
            startInsert(token, profile, ProfilesContract.CONTENT_URI, profile.toContentValues());
        }

        public void startUpdate(int token, Profile profile) {
            if (profile == null || profile.getId() == null) return;
            Uri uri = ProfilesContract.CONTENT_URI.buildUpon().appendPath(String.valueOf(profile.getId())).build();
            startUpdate(token, profile, uri, profile.toContentValues(), null, null);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if (cursor.moveToFirst()) {
                setProfile(new Profile(cursor));
            }
        }

        @Override
        protected void onInsertComplete(int token, Object cookie, Uri uri) {
            long profileId = Long.valueOf(uri.getLastPathSegment());
            Profile profile = (Profile) cookie;
            profile.setId(profileId);
            setProfile(profile);
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            Profile profile = (Profile) cookie;

            SharedPreferences.Editor editPrefs = getSharedPreferences(TrackerApplication.APP_PREFS, MODE_PRIVATE).edit();
            editPrefs.putString(TrackerApplication.PREFS_USERNAME, profile.getUserName());
            editPrefs.apply();

            switch(token) {
                case TOKEN_TRACK:
                    Intent intent = new Intent(ProfileActivity.this, TrackActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra(TrackActivity.EXTRA_PROFILE, profile);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    }
}
