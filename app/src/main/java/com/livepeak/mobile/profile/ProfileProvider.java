package com.livepeak.mobile.profile;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.livepeak.mobile.app.TrackerApplication;

public class ProfileProvider extends ContentProvider {

    private static final int PROFILE_TYPE = 10;

    private static final int PROFILE_ITEM_TYPE = 20;

    private static final int CONTACT_TYPE = 30;

    private static final int CONTACT_ITEM_TYPE = 40;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private ProfileDatasource mDatasource;

    static {
        sURIMatcher.addURI(ProfilesContract.AUTHORITY, ProfilesContract.BASE_PATH, PROFILE_TYPE);
        sURIMatcher.addURI(ProfilesContract.AUTHORITY, ProfilesContract.BASE_PATH + "/#", PROFILE_ITEM_TYPE);
        sURIMatcher.addURI(ProfilesContract.AUTHORITY, ProfilesContract.Contact.BASE_PATH, CONTACT_TYPE);
        sURIMatcher.addURI(ProfilesContract.AUTHORITY, ProfilesContract.Contact.BASE_PATH + "/#", CONTACT_ITEM_TYPE);
    }

    @Override
    public boolean onCreate() {
        mDatasource = ((TrackerApplication) getContext().getApplicationContext()).getProfileDatasource();
        return false;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case PROFILE_TYPE:
                queryBuilder.setTables(ProfileDatasource.TABLE_PROFILE);
                break;
            case PROFILE_ITEM_TYPE:
                queryBuilder.setTables(ProfileDatasource.TABLE_PROFILE);
                queryBuilder.appendWhere(ProfileDatasource.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case CONTACT_TYPE:
                queryBuilder.setTables(ProfileDatasource.TABLE_CONTACT);
                break;
            case CONTACT_ITEM_TYPE:
                queryBuilder.setTables(ProfileDatasource.TABLE_CONTACT);
                queryBuilder.appendWhere(ProfileDatasource.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = mDatasource.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);

        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long id;

        SQLiteDatabase db = mDatasource.getWritableDatabase();

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case PROFILE_TYPE:
                id = db.insertOrThrow(ProfileDatasource.TABLE_PROFILE, null, values);
                break;
            case CONTACT_TYPE:
                id = db.insertOrThrow(ProfileDatasource.TABLE_CONTACT, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        // notify content resolver
        getContext().getContentResolver().notifyChange(uri, null);

        return Uri.withAppendedPath(uri, Long.toString(id));
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int rowsUpdated;

        SQLiteDatabase db = mDatasource.getWritableDatabase();

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case PROFILE_TYPE:
                rowsUpdated = db.update(ProfileDatasource.TABLE_PROFILE, values, selection, selectionArgs);
                break;
            case PROFILE_ITEM_TYPE:
                String id = uri.getLastPathSegment();
                rowsUpdated = db.update(ProfileDatasource.TABLE_PROFILE, values, ProfileDatasource.COLUMN_ID + "=" + id, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        // notify content resolver
        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int rowsDeleted;

        SQLiteDatabase db = mDatasource.getWritableDatabase();

        int uriType = sURIMatcher.match(uri);
        String id;
        switch (uriType) {
            case PROFILE_TYPE:
                rowsDeleted = db.delete(ProfileDatasource.TABLE_PROFILE, selection, selectionArgs);
                break;
            case PROFILE_ITEM_TYPE:
                id = uri.getLastPathSegment();
                rowsDeleted = db.delete(ProfileDatasource.TABLE_PROFILE, ProfileDatasource.COLUMN_ID + "=" + id, null);
                break;
            case CONTACT_TYPE:
                rowsDeleted = db.delete(ProfileDatasource.TABLE_CONTACT, selection, selectionArgs);
                break;
            case CONTACT_ITEM_TYPE:
                id = uri.getLastPathSegment();
                rowsDeleted = db.delete(ProfileDatasource.TABLE_CONTACT, ProfileDatasource.COLUMN_ID + "=" + id, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        // notify content resolver
        getContext().getContentResolver().notifyChange(uri, null);

        return rowsDeleted;
    }
}
