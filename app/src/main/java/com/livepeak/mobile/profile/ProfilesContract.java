package com.livepeak.mobile.profile;

import android.net.Uri;

/**
 * Created by guillaume on 15/05/15.
 */
public class ProfilesContract {

    public static final String AUTHORITY = "com.livepeak.mobile.profile.provider";

    public static final String BASE_PATH = "profiles";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    public static final String NAME = "name";

    public static final String USER_NAME = "user_name";

    public static final String ACTIVITY = "activity";

    public static final String SHARING_MESSAGE = "message";

    public static final String SHARING = "sharing";

    public static final String FREQUENCY = "frequency";

    public static final String STATE = "state";

    public static final String[] PROJECTION = {"_id", NAME, USER_NAME, ACTIVITY, SHARING_MESSAGE, SHARING, FREQUENCY, STATE};

    public static final String ACTIVITY_WALKING = "Walking";

    public static final String ACTIVITY_RUNNING = "Running";

    public static final String ACTIVITY_TREKKING = "Trekking";

    public static final String ACTIVITY_BIKING = "Biking";

    public static final String ACTIVITY_MOUNTAIN_BIKING = "MountainBiking";

    public static final String ACTIVITY_SKIING = "Skiing";

    public static final String ACTIVITY_PARAGLIDING = "Paragliding";

    public static final String ACTIVITY_HANGGLIDING_ = "Hanggliding";

    public static final String ACTIVITY_CAR = "Car";

    public static final String ACTIVITY_MOTORCYCLE = "Motorcycle";

    public static final String ACTIVITY_PUBLIC_TRANSPORT = "PublicTransport";

    public static final String[] ACTIVITIES = {ACTIVITY_WALKING, ACTIVITY_RUNNING, ACTIVITY_TREKKING, ACTIVITY_BIKING,
            ACTIVITY_MOUNTAIN_BIKING, ACTIVITY_SKIING, ACTIVITY_PARAGLIDING, ACTIVITY_HANGGLIDING_,
            ACTIVITY_CAR, ACTIVITY_MOTORCYCLE, ACTIVITY_PUBLIC_TRANSPORT};

    public static final int SHARING_EMAIL = 1;

    public static final String STATE_IDLE = "Idle";

    public static final String STATE_ACTIVE = "Active";

    public static final String STATE_DELETED = "Deleted";

    public static class Contact {

        public static final String BASE_PATH = ProfilesContract.BASE_PATH + "/contacts";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(ProfilesContract.CONTENT_URI, "contacts");

        public static final String NAME = "name";

        public static final String TYPE = "type";

        public static final String VALUE = "value";

        public static final String PROFILE_ID = "profile_id";

        public static final String[] PROJECTION = {"_id", NAME, TYPE, VALUE};

        public static final String TYPE_EMAIL = "EMAIL";
    }
}
