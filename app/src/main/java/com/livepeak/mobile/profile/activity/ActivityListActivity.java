package com.livepeak.mobile.profile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.livepeak.android.view.ArrayRecyclerViewAdapter;
import com.livepeak.mobile.R;
import com.livepeak.mobile.profile.ProfileListActivity;
import com.livepeak.mobile.profile.ProfilesContract;

/**
 * Created by guillaume on 20/05/15.
 */
public class ActivityListActivity extends ActionBarActivity {

    public static final String EXTRA_ACTIVITY = "ACTIVITY";

    private RecyclerView mActivitiesRecycler;

    private String mSelectedActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity_list_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mActivitiesRecycler = (RecyclerView) findViewById(R.id.activities_recycler);
        mActivitiesRecycler.setHasFixedSize(true);
        mActivitiesRecycler.setLayoutManager(new LinearLayoutManager(this));
        mActivitiesRecycler.setAdapter(new ActivityAdapter(this, ProfilesContract.ACTIVITIES));

        Intent intent = getIntent();
        mSelectedActivity = intent.getStringExtra(EXTRA_ACTIVITY);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    upIntent = new Intent(this, ProfileListActivity.class); // can't recreate direct parent without extra
                    TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
                } else {
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ActivityAdapter extends ArrayRecyclerViewAdapter<ActivityAdapter.ViewHolder, String> {

        public ActivityAdapter(Context context, String[] activities) {
            super(context, activities);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(new ActivityListItemView(parent.getContext()));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.setActivity(getItem(position));
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private String mActivity;

            public ViewHolder(ActivityListItemView view) {
                super(view);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.putExtra(EXTRA_ACTIVITY, mActivity);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
            }

            public ActivityListItemView getView() {
                return (ActivityListItemView) itemView;
            }

            public void setActivity(String activity) {
                mActivity = activity;

                getView().setActivity(activity);
                getView().setActivated(mActivity.equals(mSelectedActivity));
            }
        }
    }
}
