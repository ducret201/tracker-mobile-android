package com.livepeak.mobile.profile;

import android.app.LoaderManager;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.livepeak.android.view.CursorRecyclerViewAdapter;
import com.livepeak.android.view.DismissView;
import com.livepeak.mobile.R;


public class ProfileListActivity extends ActionBarActivity {

    private static final int TOKEN_DELETE = 10;

    private static final int TOKEN_UNDO_DELETE = 20;

    private final static String SELECTION_PROFILES = ProfilesContract.STATE + " = ?";

    private RecyclerView mProfilesRecycler;

    private ViewGroup mProfilesEmptyLayout;

    private ProfilesAdapter mProfilesAdapter;

    private ProfileQueryHandler mProfileQueryHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_list_activity);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        LoaderCallbacks loaderCallbacks = new LoaderCallbacks();

        mProfilesAdapter = new ProfilesAdapter(this, null);
        mProfilesRecycler = (RecyclerView) findViewById(R.id.profiles_recycler);
        mProfilesRecycler.setHasFixedSize(true);
        mProfilesRecycler.setLayoutManager(layoutManager);
        mProfilesRecycler.setAdapter(mProfilesAdapter);
        mProfilesRecycler.setOnScrollListener(new OnScrollListener());
        mProfilesEmptyLayout = (ViewGroup) findViewById(R.id.profiles_empty_layout);
        mProfilesEmptyLayout.setOnClickListener(new OnClickListener());

        mProfileQueryHandler = new ProfileQueryHandler(getContentResolver());

        getLoaderManager().initLoader(0, null, loaderCallbacks);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mProfilesAdapter.flush();
        mProfilesAdapter.setNotifyDataset(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setCursor(Cursor cursor) {
        mProfilesAdapter.swapCursor(cursor);
        if (mProfilesAdapter.isNotifyDataset()) {

            if (cursor == null || cursor.getCount() == 0) {
                mProfilesRecycler.setVisibility(View.INVISIBLE);
                mProfilesEmptyLayout.setVisibility(View.VISIBLE);
            } else {
                mProfilesRecycler.setVisibility(View.VISIBLE);
                mProfilesEmptyLayout.setVisibility(View.INVISIBLE);
            }
        }
    }

    private class ProfilesAdapter extends CursorRecyclerViewAdapter<ProfilesAdapter.ViewHolder, ProfileDatasource.ProfileCursorHolder> {

        private static final int TYPE_PROFILE = 10;

        private int mDelayedRemovedPosition = -1;

        private ProfilesAdapter(Context context, Cursor cursor) {
            super(context, cursor);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(new ProfileListItemView(parent.getContext()));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, Cursor cursor, ProfileDatasource.ProfileCursorHolder cursorHolder) {
            holder.setProfile(cursorHolder);
        }

        @Override
        protected ProfileDatasource.ProfileCursorHolder onCreateCursorHolder(Cursor cursor) {
            return new ProfileDatasource.ProfileCursorHolder(cursor);
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_PROFILE;
        }

        public void remove(int position) {
            if (mDelayedRemovedPosition >= 0) {
                notifyItemRemoved(mDelayedRemovedPosition);

                if (mDelayedRemovedPosition < position) {
                    position--;
                }
            }
            mDelayedRemovedPosition = position;
        }

        public void undoRemove(int position) {
            if (mDelayedRemovedPosition == position) {
                mDelayedRemovedPosition = -1;
            }
        }

        public void flush() {
            if (mDelayedRemovedPosition >= 0) {
                notifyItemRemoved(mDelayedRemovedPosition);
                mDelayedRemovedPosition = -1;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private long mProfileId;

            public ViewHolder(ProfileListItemView view) {
                super(view);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ProfileListActivity.this, ProfileActivity.class);
                        intent.putExtra(ProfileActivity.EXTRA_PROFILE_ID, mProfileId);
                        startActivity(intent);
                    }
                });

                view.setOnDismissListener(new DismissView.OnDismissListener() {
                    @Override
                    public void onDismiss(DismissView view) {
                        mProfilesAdapter.setNotifyDataset(false);
                        mProfileQueryHandler.startUpdateState(TOKEN_DELETE, ViewHolder.this, mProfileId, ProfilesContract.STATE_DELETED);
                    }

                    @Override
                    public void onUndo(DismissView view) {
                        mProfilesAdapter.setNotifyDataset(false);
                        mProfileQueryHandler.startUpdateState(TOKEN_UNDO_DELETE, ViewHolder.this, mProfileId, ProfilesContract.STATE_ACTIVE);
                    }
                });
            }

            public ProfileListItemView getView() {
                return (ProfileListItemView) itemView;
            }

            public void setProfile(ProfileDatasource.ProfileCursorHolder cursorHolder) {
                Cursor cursor = cursorHolder.getCursor();
                mProfileId = cursor.getLong(cursorHolder.idIndex);

                getView().setProfile(cursorHolder);
            }
        }
    }

    private class LoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            String[] projection = {ProfileDatasource.COLUMN_ID, ProfilesContract.NAME, ProfilesContract.ACTIVITY, ProfilesContract.STATE};
            String[] selectionArgs = {ProfilesContract.STATE_ACTIVE};
            CursorLoader cursorLoader = new CursorLoader(ProfileListActivity.this, ProfilesContract.CONTENT_URI, projection, SELECTION_PROFILES, selectionArgs, null);
            return cursorLoader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            setCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            setCursor(null);
        }
    }

    private class ProfileQueryHandler extends AsyncQueryHandler {

        public ProfileQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void startUpdateState(int token, ProfilesAdapter.ViewHolder viewHolder, long profileId, String state) {
            Uri uri = ProfilesContract.CONTENT_URI.buildUpon().appendPath(String.valueOf(profileId)).build();
            ContentValues values = new ContentValues();
            values.put(ProfilesContract.STATE, state);
            super.startUpdate(token, viewHolder, uri, values, null, null);
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            ProfilesAdapter.ViewHolder viewHolder = (ProfilesAdapter.ViewHolder) cookie;

            switch (token) {
                case TOKEN_DELETE:
                    mProfilesAdapter.remove(viewHolder.getAdapterPosition());
                    viewHolder.getView().allowUndo();
                    break;
                case TOKEN_UNDO_DELETE:
                    mProfilesAdapter.undoRemove(viewHolder.getAdapterPosition());
                    viewHolder.getView().reset();
                    break;
            }
        }
    }

    private class OnScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            mProfilesAdapter.flush();
            mProfilesAdapter.setNotifyDataset(true);
        }
    }

    private class OnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.profiles_empty_layout:
                    Intent intent = new Intent(ProfileListActivity.this, ProfileActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    }
}
